#pragma once

#include "pin.h"
#include "avr_ports.h"

#if defined(__AVR_ATtiny84A__)

// ATtiny84A Pin

typedef Pin<PortA,0> pin_a0;
typedef Pin<PortA,1> pin_a1;
typedef Pin<PortA,2> pin_a2;
typedef Pin<PortA,3> pin_a3;
typedef Pin<PortA,4> pin_a4;
typedef Pin<PortA,5> pin_a5;
typedef Pin<PortA,6> pin_a6;
typedef Pin<PortA,7> pin_a7;

typedef Pin<PortB,0> pin_b0;
typedef Pin<PortB,1> pin_b1;
typedef Pin<PortB,2> pin_b2;
typedef Pin<PortB,3> pin_b3;

#endif // endif


#if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega328__)

// ATMEGA328P Pin

typedef Pin<PortD,0> pin_d0;
typedef Pin<PortD,1> pin_d1;
typedef Pin<PortD,2> pin_d2;
typedef Pin<PortD,3> pin_d3;
typedef Pin<PortD,4> pin_d4;
typedef Pin<PortD,5> pin_d5;
typedef Pin<PortD,6> pin_d6;
typedef Pin<PortD,7> pin_d7;

typedef Pin<PortB,0> pin_b0;
typedef Pin<PortB,1> pin_b1;
typedef Pin<PortB,2> pin_b2;
typedef Pin<PortB,3> pin_b3;
typedef Pin<PortB,4> pin_b4;
typedef Pin<PortB,5> pin_b5;
typedef Pin<PortB,6> pin_b6;
typedef Pin<PortB,7> pin_b7;

#endif // endif

#ifdef _ARDUINO_UNO_
// arduino uno Pin

#ifndef __AVR_ATmega328P__
#error Arduino uno requires ATMega328P
#endif

typedef pin_d1 pin_1;
typedef pin_d2 pin_2;
typedef pin_d3 pin_3;
typedef pin_d4 pin_4;
typedef pin_d5 pin_5;
typedef pin_d6 pin_6;
typedef pin_d7 pin_7;

typedef pin_b0 pin_8;
typedef pin_b1 pin_9;
typedef pin_b2 pin_10;
typedef pin_b3 pin_11;
typedef pin_b4 pin_12;
typedef pin_b5 pin_13;
#endif // _ARDUINO_UNO_


