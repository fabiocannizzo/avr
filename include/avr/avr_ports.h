#pragma once

#include <avr/io.h>

#include <mem_addr.h> // this is generated on the fly by the makefile

struct PortA
{
#ifdef DDRA
    static constexpr uintptr_t ddr = __autogen__DDRA;
#endif
#ifdef PORTA
    static constexpr uintptr_t port = __autogen__PORTA;
#endif
#ifdef PINA
    static constexpr uintptr_t pin = __autogen__PINA;
#endif
};

struct PortB
{
#ifdef DDRB
    static constexpr uintptr_t ddr = __autogen__DDRB;
#endif
#ifdef PORTB
    static constexpr uintptr_t port = __autogen__PORTB;
#endif
#ifdef PINB
    static constexpr uintptr_t pin = __autogen__PINB;
#endif
};

struct PortC
{
#ifdef DDRC
    static constexpr uintptr_t ddr = __autogen__DDRC;
#endif
#ifdef PORTC
    static constexpr uintptr_t port = __autogen__PORTC;
#endif
#ifdef PINC
    static constexpr uintptr_t pin = __autogen__PINC;
#endif
};

struct PortD
{
#ifdef DDRD
    static constexpr uintptr_t ddr = __autogen__DDRD;
#endif
#ifdef PORTD
    static constexpr uintptr_t port = __autogen__PORTD;
#endif
#ifdef PIND
    static constexpr uintptr_t pin = __autogen__PIND;
#endif
};
