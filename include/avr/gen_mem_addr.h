#include <avr/io.h>

#ifdef PORTA
constexpr uintptr_t __autogen__PORTA = PORTA;
#endif

#ifdef DDRA
constexpr uintptr_t __autogen__DDRA = DDRA;
#endif

#ifdef PINA
constexpr uintptr_t __autogen__PINA = PINA;
#endif

#ifdef PORTB
constexpr uintptr_t __autogen__PORTB = PORTB;
#endif

#ifdef DDRB
constexpr uintptr_t __autogen__DDRB = DDRB;
#endif

#ifdef PINB
constexpr uintptr_t __autogen__PINB = PINB;
#endif

#ifdef PORTC
constexpr uintptr_t __autogen__PORTC = PORTC;
#endif

#ifdef DDRC
constexpr uintptr_t __autogen__DDRC = DDRC;
#endif

#ifdef PINC
constexpr uintptr_t __autogen__PINC = PINC;
#endif

#ifdef PORTD
constexpr uintptr_t __autogen__PORTD = PORTD;
#endif

#ifdef DDRD
constexpr uintptr_t __autogen__DDRD = DDRD;
#endif

#ifdef PIND
constexpr uintptr_t __autogen__PIND = PIND;
#endif
