#pragma once

#include "bit_utils.h"

#include <stddef.h>
#include <string.h>

/*
   Constructor examples:

     // an array of 10 bits not initialized
     bit_array<10> a{};

     // an array of 10 bits initialized to 0
     bit_array<10> a{0};

     // an array of 10 bits initialized to 1
     bit_array<10> a{1};

     // an array of 5 bits initialized as per string
     bit_array<5> a{"01110"};

     // an array of 5 bits initialized as per array
     uint8_t v[5] = {1, 0, 0, 1, 1};
     bit_array<5> a{v};

     // an array of 9 bits initialized to {1,0,0,0,1,1,1,1,1,0}
     // this is a 'packed' initialization, i.e. the individual bits
     // are extracted from the given 8-bit values
     bit_array<10> a{ {0x8F, 0x80} };
*/

template <uint32_t N>
struct bit_array
{
    // number of bytes necessary to contain n bits
    static constexpr uint32_t NB = calc_n_bytes(N);

    using length_t = smallest_uint_t<N>;
    using nbytes_t = smallest_uint_t<NB>;

    static constexpr length_t length = static_cast<length_t>(N);
    static constexpr nbytes_t nbytes = static_cast<nbytes_t>(NB);

    static constexpr length_t index(length_t i) { return NB > 1 ? i / 8: 0; }
    static constexpr uint8_t mask(length_t i) { return bit8(NB > 1 ? i % 8: i); }

    static_assert(sizeof(nbytes_t) <= sizeof(size_t), "Cannot allocate an array so large with this instruction set");

    struct bit_pack
    {
        uint8_t m_bytes[nbytes];
    };

    bit_array() {}

    bit_array(bool v) { set_all(v); }

    bit_array(const char *bit_string)
    {
       for (length_t i = 0; i < length; ++i)
           set(i, bit_string[i] - '0');
    }

    template <typename IntType>
    bit_array(const IntType *ints)
    {
        for (length_t i = 0; i < length; ++i)
            set(i, ints[i]);
    }

    bit_array(const bit_pack&& values)
        : m_data(values)
    {
    }

    bool get(length_t i) const
    {
        return m_data.m_bytes[index(i)] & mask(i);
    }

    void set(length_t i, bool v)
    {
       if (v)
          m_data.m_bytes[index(i)] |= mask(i);
       else
          m_data.m_bytes[index(i)] &= ~mask(i);
    }

    void set_all(bool v)
    {
        const uint8_t mask = v ? 0xFF: 0;
        memset(m_data.m_bytes, mask, nbytes);
    }

    void set_byte(nbytes_t b, uint8_t v)
    {
        m_data.m_bytes[b] = v;
    }

    uint8_t get_byte(nbytes_t b) const
    {
        return m_data.m_bytes[b];
    }

    uint8_t *data() { return m_data.m_bytes; }
    const uint8_t *data() const { return m_data.m_bytes; }

private:
    bit_pack m_data;
};


namespace test {
    // check nbytes calculation
    static_assert(bit_array<1>::nbytes == 1);
    static_assert(bit_array<8>::nbytes == 1);
    static_assert(bit_array<9>::nbytes == 2);
    static_assert(bit_array<16>::nbytes == 2);
    static_assert(bit_array<17>::nbytes == 3);

    // check index calculation
    static_assert(bit_array<1>::index(0) == 0);
    static_assert(bit_array<8>::index(7) == 0);
    static_assert(bit_array<9>::index(8) == 1);
    static_assert(bit_array<16>::index(15) == 1);
    static_assert(bit_array<17>::index(16) == 2);

    // check mask calculation
    static_assert(bit_array<1>::mask(0) == bit8(0));
    static_assert(bit_array<8>::mask(7) == bit8(7));
    static_assert(bit_array<9>::mask(8) == bit8(0));
    static_assert(bit_array<16>::mask(15) == bit8(7));
    static_assert(bit_array<17>::mask(16) == bit8(0));

    // check length_t definition
    static_assert(std::is_same<bit_array<0xFFul+1>::length_t,uint8_t>::value);
    static_assert(std::is_same<bit_array<0xFFul+2>::length_t,uint16_t>::value);
    static_assert(std::is_same<bit_array<0xFFFFul+1>::length_t,uint16_t>::value);
    static_assert(std::is_same<bit_array<0xFFFFul+2>::length_t,uint32_t>::value);

    // check nbytes_t definition
    static_assert(std::is_same<bit_array<8*(1+0xFFul)>::nbytes_t,uint8_t>::value);
    static_assert(std::is_same<bit_array<8*(1+0xFFul)+1>::nbytes_t,uint16_t>::value);
    static_assert(std::is_same<bit_array<8*(1+0xFFFFul)>::nbytes_t,uint16_t>::value);
}  // namespace test


