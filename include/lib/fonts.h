#pragma once

#include "pgmspacewrap.h"

class FontChar
{
    const uint8_t *m_cols;
public:
    FontChar(const uint8_t *cols) : m_cols(cols) {}

    inline uint8_t operator[](uint8_t i) const
    {
        return pgm_read_byte(&m_cols[i]);
    }
};

template <char>
inline FontChar ledFont7x5Cols();

// space
template <>
inline FontChar ledFont7x5Cols<' '>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b00000000,0b00000000,0b00000000,0b00000000};
    return FontChar(cols);
}

// !
template <>
inline FontChar ledFont7x5Cols<'!'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b00000000,0b01001111,0b00000000,0b00000000};
    return FontChar(cols);
}

// "
template <>
inline FontChar ledFont7x5Cols<'"'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b00000111,0b00000000,0b00000111,0b00000000};
    return FontChar(cols);
}

// #
template <>
inline FontChar ledFont7x5Cols<'#'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00010100,0b01111111,0b00010100,0b01111111,0b00010100};
    return FontChar(cols);
}

// $
template <>
inline FontChar ledFont7x5Cols<'$'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00100100,0b00101010,0b01111111,0b00101010,0b00010010};
    return FontChar(cols);
}

// %
template <>
inline FontChar ledFont7x5Cols<'%'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00100011,0b00010011,0b00001000,0b01100100,0b01100010};
    return FontChar(cols);
}

// &
template <>
inline FontChar ledFont7x5Cols<'&'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00110110,0b01001001,0b01010101,0b00100010,0b01010000};
    return FontChar(cols);
}

// '
template <>
inline FontChar ledFont7x5Cols<'\''>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b00000101,0b00000011,0b00000000,0b00000000};
    return FontChar(cols);
}

// (
template <>
inline FontChar ledFont7x5Cols<'('>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b00011100,0b00100010,0b01000001,0b00000000};
    return FontChar(cols);
}

// )
template <>
inline FontChar ledFont7x5Cols<')'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b01000001,0b00100010,0b00011100,0b00000000};
    return FontChar(cols);
}

// *
template <>
inline FontChar ledFont7x5Cols<'*'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00010100,0b00001000,0b00111110,0b00001000,0b00010100};
    return FontChar(cols);
}

// +
template <>
inline FontChar ledFont7x5Cols<'+'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001000,0b00001000,0b00111110,0b00001000,0b00001000};
    return FontChar(cols);
}

// ,
template <>
inline FontChar ledFont7x5Cols<','>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b01010000,0b00110000,0b00000000,0b00000000};
    return FontChar(cols);
}

// -
template <>
inline FontChar ledFont7x5Cols<'-'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001000,0b00001000,0b00001000,0b00001000,0b00001000};
    return FontChar(cols);
}

// .
template <>
inline FontChar ledFont7x5Cols<'.'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b01100000,0b01100000,0b00000000,0b00000000};
    return FontChar(cols);
}

// /
template <>
inline FontChar ledFont7x5Cols<'/'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00100000,0b00010000,0b00001000,0b00000100,0b00000010};
    return FontChar(cols);
}

// 0
template <>
inline FontChar ledFont7x5Cols<'0'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111110,0b01010001,0b01001001,0b01000101,0b00111110};
    return FontChar(cols);
}

// 1
template <>
inline FontChar ledFont7x5Cols<'1'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b01000010,0b01111111,0b01000000,0b00000000};
    return FontChar(cols);
}

// 2
template <>
inline FontChar ledFont7x5Cols<'2'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01000010,0b01100001,0b01010001,0b01001001,0b01000110};
    return FontChar(cols);
}

// 3
template <>
inline FontChar ledFont7x5Cols<'3'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00100001,0b01000001,0b01000101,0b01001011,0b00110001};
    return FontChar(cols);
}

// 4
template <>
inline FontChar ledFont7x5Cols<'4'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00011000,0b00010100,0b00010010,0b01111111,0b00010000};
    return FontChar(cols);
}

// 5
template <>
inline FontChar ledFont7x5Cols<'5'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00100111,0b01000101,0b01000101,0b01000101,0b00111001};
    return FontChar(cols);
}

// 6
template <>
inline FontChar ledFont7x5Cols<'6'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111100,0b01001010,0b01001001,0b01001001,0b00110000};
    return FontChar(cols);
}

// 7
template <>
inline FontChar ledFont7x5Cols<'7'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000011,0b01110001,0b00001001,0b00000101,0b00000011};
    return FontChar(cols);
}

// 8
template <>
inline FontChar ledFont7x5Cols<'8'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00110110,0b01001001,0b01001001,0b01001001,0b00110110};
    return FontChar(cols);
}

// 9
template <>
inline FontChar ledFont7x5Cols<'9'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000110,0b01001001,0b01001001,0b00101001,0b00011110};
    return FontChar(cols);
}

// :
template <>
inline FontChar ledFont7x5Cols<':'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b01101100,0b01101100,0b00000000,0b00000000};
    return FontChar(cols);
}

// ;
template <>
inline FontChar ledFont7x5Cols<';'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b01010110,0b00110110,0b00000000,0b00000000};
    return FontChar(cols);
}

// <
template <>
inline FontChar ledFont7x5Cols<'<'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001000,0b00010100,0b00100010,0b01000001,0b00000000};
    return FontChar(cols);
}

// =
template <>
inline FontChar ledFont7x5Cols<'='>()
{
    static const uint8_t cols[5] PROGMEM = {0b00010100,0b00010100,0b00010100,0b00010100,0b00010100};
    return FontChar(cols);
}

// >
template <>
inline FontChar ledFont7x5Cols<'>'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b01000001,0b00100010,0b00010100,0b00001000};
    return FontChar(cols);
}

// ?
template <>
inline FontChar ledFont7x5Cols<'?'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000010,0b00000001,0b01010001,0b00001001,0b00000110};
    return FontChar(cols);
}

// @
template <>
inline FontChar ledFont7x5Cols<'@'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00110010,0b01001001,0b01111001,0b01000001,0b00111110};
    return FontChar(cols);
}

// A
template <>
inline FontChar ledFont7x5Cols<'A'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111110,0b00010001,0b00010001,0b00010001,0b01111110};
    return FontChar(cols);
}

// B
template <>
inline FontChar ledFont7x5Cols<'B'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b01001001,0b01001001,0b01001001,0b00111110};
    return FontChar(cols);
}

// C
template <>
inline FontChar ledFont7x5Cols<'C'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111110,0b01000001,0b01000001,0b01000001,0b00100010};
    return FontChar(cols);
}

// D
template <>
inline FontChar ledFont7x5Cols<'D'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b01000001,0b01000001,0b01000001,0b00111110};
    return FontChar(cols);
}

// E
template <>
inline FontChar ledFont7x5Cols<'E'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b01001001,0b01001001,0b01001001,0b01001001};
    return FontChar(cols);
}

// F
template <>
inline FontChar ledFont7x5Cols<'F'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b00001001,0b00001001,0b00001001,0b00000001};
    return FontChar(cols);
}

// G
template <>
inline FontChar ledFont7x5Cols<'G'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111110,0b01000001,0b01001001,0b01001001,0b00111010};
    return FontChar(cols);
}

// H
template <>
inline FontChar ledFont7x5Cols<'H'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b00001000,0b00001000,0b00001000,0b01111111};
    return FontChar(cols);
}

// I
template <>
inline FontChar ledFont7x5Cols<'I'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01000001,0b01000001,0b01111111,0b01000001,0b01000001};
    return FontChar(cols);
}

// J
template <>
inline FontChar ledFont7x5Cols<'J'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00110000,0b01000001,0b01000001,0b00111111,0b00000001};
    return FontChar(cols);
}

// K
template <>
inline FontChar ledFont7x5Cols<'K'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b00001000,0b00010100,0b00100010,0b01000001};
    return FontChar(cols);
}

// L
template <>
inline FontChar ledFont7x5Cols<'L'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b01000000,0b01000000,0b01000000,0b01000000};
    return FontChar(cols);
}

// M
template <>
inline FontChar ledFont7x5Cols<'M'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b00000010,0b00001100,0b00000010,0b01111111};
    return FontChar(cols);
}

// N
template <>
inline FontChar ledFont7x5Cols<'N'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b00000100,0b00001000,0b00010000,0b01111111};
    return FontChar(cols);
}

// O
template <>
inline FontChar ledFont7x5Cols<'O'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111110,0b01000001,0b01000001,0b01000001,0b00111110};
    return FontChar(cols);
}

// P
template <>
inline FontChar ledFont7x5Cols<'P'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b00001001,0b00001001,0b00001001,0b00000110};
    return FontChar(cols);
}

// Q
template <>
inline FontChar ledFont7x5Cols<'Q'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111110,0b01000001,0b01010001,0b00100001,0b01011110};
    return FontChar(cols);
}

// R
template <>
inline FontChar ledFont7x5Cols<'R'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b00001001,0b00001001,0b00011001,0b01100110};
    return FontChar(cols);
}

// S
template <>
inline FontChar ledFont7x5Cols<'S'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01000110,0b01001001,0b01001001,0b01001001,0b00110001};
    return FontChar(cols);
}

// T
template <>
inline FontChar ledFont7x5Cols<'T'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000001,0b00000001,0b01111111,0b00000001,0b00000001};
    return FontChar(cols);
}

// U
template <>
inline FontChar ledFont7x5Cols<'U'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111111,0b01000000,0b01000000,0b01000000,0b00111111};
    return FontChar(cols);
}

// V
template <>
inline FontChar ledFont7x5Cols<'V'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001111,0b00110000,0b01000000,0b00110000,0b00001111};
    return FontChar(cols);
}

// W
template <>
inline FontChar ledFont7x5Cols<'W'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111111,0b01000000,0b00111000,0b01000000,0b00111111};
    return FontChar(cols);
}

// X
template <>
inline FontChar ledFont7x5Cols<'X'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01100011,0b00010100,0b00001000,0b00010100,0b01100011};
    return FontChar(cols);
}

// Y
template <>
inline FontChar ledFont7x5Cols<'Y'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000011,0b00000100,0b01111000,0b00000100,0b00000011};
    return FontChar(cols);
}

// Z
template <>
inline FontChar ledFont7x5Cols<'Z'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01100001,0b01010001,0b01001001,0b01000101,0b01000011};
    return FontChar(cols);
}

// [
template <>
inline FontChar ledFont7x5Cols<'['>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b01000001,0b01000001,0b00000000,0b00000000};
    return FontChar(cols);
}

// '\'
template <>
inline FontChar ledFont7x5Cols<'\\'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000010,0b00000100,0b00001000,0b00010000,0b00100000};
    return FontChar(cols);
}

// ]
template <>
inline FontChar ledFont7x5Cols<']'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b00000000,0b01000001,0b01000001,0b01111111};
    return FontChar(cols);
}

// ^
template <>
inline FontChar ledFont7x5Cols<'^'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000100,0b00000010,0b00000001,0b00000010,0b00000100};
    return FontChar(cols);
}

// _
template <>
inline FontChar ledFont7x5Cols<'_'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01000000,0b01000000,0b01000000,0b01000000,0b01000000};
    return FontChar(cols);
}

// `
template <>
inline FontChar ledFont7x5Cols<'`'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b00000001,0b00000010,0b00000100,0b00000000};
    return FontChar(cols);
}

// a
template <>
inline FontChar ledFont7x5Cols<'a'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00100000,0b01010100,0b01010100,0b01010100,0b01111000};
    return FontChar(cols);
}

// 0b
template <>
inline FontChar ledFont7x5Cols<'b'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b01001000,0b01000100,0b01000100,0b00111000};
    return FontChar(cols);
}

// c
template <>
inline FontChar ledFont7x5Cols<'c'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111000,0b01000100,0b01000100,0b01000100,0b00100000};
    return FontChar(cols);
}

// d
template <>
inline FontChar ledFont7x5Cols<'d'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111000,0b01000100,0b01000100,0b01001000,0b01111111};
    return FontChar(cols);
}

// e
template <>
inline FontChar ledFont7x5Cols<'e'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111000,0b01010100,0b01010100,0b01010100,0b00011000};
    return FontChar(cols);
}

// f
template <>
inline FontChar ledFont7x5Cols<'f'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001000,0b01111110,0b00001001,0b00000001,0b00000010};
    return FontChar(cols);
}

// g
template <>
inline FontChar ledFont7x5Cols<'g'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001100,0b01010010,0b01010010,0b01010010,0b00111110};
    return FontChar(cols);
}

// h
template <>
inline FontChar ledFont7x5Cols<'h'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b00001000,0b00000100,0b00000100,0b01111000};
    return FontChar(cols);
}

// i
template <>
inline FontChar ledFont7x5Cols<'i'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b01000100,0b01111101,0b01000000,0b00000000};
    return FontChar(cols);
}

// j
template <>
inline FontChar ledFont7x5Cols<'j'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00100000,0b01000000,0b01000100,0b00111101,0b00000000};
    return FontChar(cols);
}

// k
template <>
inline FontChar ledFont7x5Cols<'k'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111111,0b00010000,0b00101000,0b01000100,0b00000000};
    return FontChar(cols);
}

// l
template <>
inline FontChar ledFont7x5Cols<'l'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b01000001,0b01111111,0b01000000,0b00000000};
    return FontChar(cols);
}

// m
template <>
inline FontChar ledFont7x5Cols<'m'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111000,0b00000100,0b00001000,0b00000100,0b01111000};
    return FontChar(cols);
}

// n
template <>
inline FontChar ledFont7x5Cols<'n'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111100,0b00001000,0b00000100,0b00000100,0b01111000};
    return FontChar(cols);
}

// o
template <>
inline FontChar ledFont7x5Cols<'o'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111000,0b01000100,0b01000100,0b01000100,0b00111000};
    return FontChar(cols);
}

// p
template <>
inline FontChar ledFont7x5Cols<'p'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111100,0b00010100,0b00010100,0b00010100,0b00001000};
    return FontChar(cols);
}

// q
template <>
inline FontChar ledFont7x5Cols<'q'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001000,0b00010100,0b00010100,0b01111100,0b00000000};
    return FontChar(cols);
}

// r
template <>
inline FontChar ledFont7x5Cols<'r'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01111100,0b00001000,0b00000100,0b00000100,0b00001000};
    return FontChar(cols);
}

// s
template <>
inline FontChar ledFont7x5Cols<'s'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01001000,0b01010100,0b01010100,0b01010100,0b00100000};
    return FontChar(cols);
}

// t
template <>
inline FontChar ledFont7x5Cols<'t'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000100,0b00111111,0b01000100,0b01000000,0b00100000};
    return FontChar(cols);
}

// u
template <>
inline FontChar ledFont7x5Cols<'u'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111100,0b01000000,0b01000000,0b00100000,0b01111100};
    return FontChar(cols);
}

// v
template <>
inline FontChar ledFont7x5Cols<'v'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00011100,0b00100000,0b01000000,0b00100000,0b00011100};
    return FontChar(cols);
}

// w
template <>
inline FontChar ledFont7x5Cols<'w'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111100,0b01000000,0b00110000,0b01000000,0b00111100};
    return FontChar(cols);
}

// x
template <>
inline FontChar ledFont7x5Cols<'x'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01000100,0b00101000,0b00010000,0b00101000,0b01000100};
    return FontChar(cols);
}

// y
template <>
inline FontChar ledFont7x5Cols<'y'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001100,0b01010000,0b01010000,0b01010000,0b00111100};
    return FontChar(cols);
}

// z
template <>
inline FontChar ledFont7x5Cols<'z'>()
{
    static const uint8_t cols[5] PROGMEM = {0b01000100,0b01100100,0b01010100,0b01001100,0b01000100};
    return FontChar(cols);
}

// {
template <>
inline FontChar ledFont7x5Cols<'{'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b00001000,0b00110110,0b01000001,0b00000000};
    return FontChar(cols);
}

// |
template <>
inline FontChar ledFont7x5Cols<'|'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b00000000,0b01111111,0b00000000,0b00000000};
    return FontChar(cols);
}

// }
template <>
inline FontChar ledFont7x5Cols<'}'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00000000,0b01000001,0b00110110,0b00001000,0b00000000};
    return FontChar(cols);
}

// ~
template <>
inline FontChar ledFont7x5Cols<'~'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001000,0b00000100,0b00000100,0b00001000,0b00000100};
    return FontChar(cols);
}

// infinite
template <>
inline FontChar ledFont7x5Cols<'\236'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001100,0b00010010,0b00001100,0b00010010,0b00001100};
    return FontChar(cols);
}

// square
template <>
inline FontChar ledFont7x5Cols<'\254'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00111110,0b00100010,0b00100010,0b00100010,0b00111110};
    return FontChar(cols);
}

// left-arrow
template <>
inline FontChar ledFont7x5Cols<'\250'>()
{
    static const uint8_t cols[5] PROGMEM = {0b00001000,0b00011100,0b00101010,0b00001000,0b00001000};
    return FontChar(cols);
}
