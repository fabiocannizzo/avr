#pragma once

#include "pin.h"
#include "assert_util.h"

#include <type_traits>
#include <tuple>

namespace multi_pin_h {

template <uint8_t INDEX, uint8_t MASK>
struct ArgInfo
{
    static constexpr uint8_t index = INDEX;
    static constexpr uint8_t mask = MASK;
};

template <typename PORT, typename...ArgInfo>
struct PortInfoAux
{
    using port_t = PORT;
};

template < typename PORT
         , uint8_t MASK  // bitwise or of all pin masks
         , uint8_t...ArgIndex  // argument index in set_values, ordered from ther LSB to the MSB in MASK
         >
struct PortInfo
{
    using port_t = PORT;
    static constexpr uint8_t mask = MASK;
    static constexpr uint8_t npins = n_active_bits(mask);
};

template <typename...PORTINFOS>
struct PortInfoPack
{
    static constexpr uint8_t nPorts = sizeof...(PORTINFOS);
};

//
// Sort indices of arguments based on the position in the mask
//


template <uint8_t MASKMIN, typename MinSoFar, typename...ArgInfos>
struct MinLargest
{
    using type_t = MinSoFar;
};

template <uint8_t MASKMIN, typename MinSoFar, typename ArgInfoHead, typename...ArgInfoTail>
struct MinLargest<MASKMIN, MinSoFar, ArgInfoHead, ArgInfoTail...>
{
    using new_min_t = typename std::conditional
        < ((ArgInfoHead::mask > MASKMIN) && (ArgInfoHead::mask < MinSoFar::mask))
        , ArgInfoHead
        , MinSoFar
        >::type;
    using type_t = typename MinLargest<MASKMIN, new_min_t, ArgInfoTail...>::type_t;
};

template <uint8_t N, typename PI, uint8_t MASKMIN, typename...ArgInfos>
struct ArgSorterImpl;

template <uint8_t N, typename PORT, uint8_t MASK, uint8_t...ArgIndices, uint8_t MASKMIN, typename...ArgInfos>
struct ArgSorterImpl<N, PortInfo<PORT,MASK, ArgIndices...>, MASKMIN, ArgInfos...>
{
    using min_t = typename MinLargest<MASKMIN, ArgInfo<0xFF,0xFF>, ArgInfos...>::type_t;
    using type_t = typename ArgSorterImpl<N-1, PortInfo<PORT,MASK,ArgIndices...,min_t::index>, min_t::mask, ArgInfos...>::type_t;
};

template <typename PORT, uint8_t MASK, uint8_t...ArgIndices, uint8_t MASKMIN, typename...ArgInfos>
struct ArgSorterImpl<0, PortInfo<PORT,MASK, ArgIndices...>, MASKMIN, ArgInfos...>
{
    using type_t = PortInfo<PORT,MASK,ArgIndices...>;
};


//
// MakePortInfo
//

template <uint8_t ArgIndex, typename PIX, typename...PINS>
struct MakePortInfo;

template <uint8_t ArgIndex, typename PORT, typename...ArgInfos>
struct MakePortInfo<ArgIndex, PortInfoAux<PORT, ArgInfos...>>
{
    static constexpr uint8_t mask = (ArgInfos::mask | ...);
    static_assert(sizeof...(ArgInfos) == n_active_bits(mask), "Duplicate pin in MultiPin declaration");
    using type_t = typename ArgSorterImpl<sizeof...(ArgInfos), PortInfo<PORT, mask>, 0, ArgInfos...>::type_t;
};

template <uint8_t ArgIndex, typename PORT, typename...ArgInfos, typename PIN_HEAD, typename...PIN_TAIL>
struct MakePortInfo<ArgIndex, PortInfoAux<PORT, ArgInfos...>, PIN_HEAD, PIN_TAIL...>
{
    static constexpr bool pin_in_this_port = std::is_same<typename PIN_HEAD::port_t, PORT>::value;
    using new_port_info_t = typename std::conditional
        < pin_in_this_port
        , PortInfoAux<PORT, ArgInfos..., ArgInfo<ArgIndex, PIN_HEAD::mask>>  // add pin to PortInfo
        , PortInfoAux<PORT, ArgInfos...>                                      // PortInfo unchanged
        >::type;
    using type_t = typename MakePortInfo<ArgIndex + 1, new_port_info_t, PIN_TAIL...>::type_t;
};


//
// MakePortInfoPacks
//


template <uint8_t ArgIndex, typename PIP, typename...PINS>
struct MakePortInfoPacks
{
    using type_t = PIP;
};

template <uint8_t ArgIndex, typename...PI, typename PIN_HEAD, typename...PIN_TAIL>
struct MakePortInfoPacks<ArgIndex, PortInfoPack<PI...>, PIN_HEAD, PIN_TAIL...>
{
    using pin_port_t = typename PIN_HEAD::port_t;
    // if the port is already in PI..., all pins on this port have already been processed
    static constexpr bool alreadyProcessed = (std::is_same<pin_port_t, typename PI::port_t>::value || ...);
    using newPortInfoPack_t = typename std::conditional
        < alreadyProcessed
        , PortInfoPack<PI...>                                                                 // PortInfoPack unchanged
        , PortInfoPack<PI..., typename MakePortInfo<ArgIndex, PortInfoAux<pin_port_t>, PIN_HEAD, PIN_TAIL...>::type_t>  // build the PortInfo for this port and add to the PortInfoPack
        >::type;
    using type_t = typename MakePortInfoPacks<ArgIndex + 1, newPortInfoPack_t, PIN_TAIL...>::type_t;

};


//
// MakeTupleTypeImpl
//

template <uint8_t N,typename...TA>
struct MakeTupleTypeImpl
{
    using type_t = typename MakeTupleTypeImpl<N-1,uint8_t,TA...>::type_t;
};

template <typename... TA>
struct MakeTupleTypeImpl<0,TA...>
{
    using type_t = std::tuple<TA...>;
};

template <uint8_t N>
struct MakeTupleType : MakeTupleTypeImpl<N>
{
};

// test MakeTupleType
static_assert(std::is_same<MakeTupleType<3>::type_t, std::tuple<uint8_t,uint8_t,uint8_t>>::value, "MakeTupleType failed");


// FIXME: is there a way to avoid defining these helper classes, and reference
// directly the static member data or functions of the templated classes
// without compromising inlining?


template <typename PORTINFO, template <uintptr_t> class MEMVAR>
struct MPHelper;

template <typename...PORTINFOS, template <uintptr_t> class MEMVAR>
struct MPHelper<PortInfoPack<PORTINFOS...>, MEMVAR>
{
    template <template <typename DP> typename ADDR, template <typename BB> typename ACTION>
    static FORCEINLINE void exec()
    {
        using foreach = int[sizeof...(PORTINFOS)];
        foreach{(ACTION<ByteBits<MEMVAR<ADDR<typename PORTINFOS::port_t>::value>,PORTINFOS::mask>>::exec(),0)...};
    }
};

} // namespace multi_pin_h


// This can handle multiple pins.
// All arguments must be of type SinglePin<PortId,BITIND>.
// Duplicates are not allowed.
// If multiple ports are involved, the order of execution
// depends on the order each port appears first in the list of pins.
template <typename PIN, typename...PINs>
struct MultiPin
{
private:
    template <class T> struct DP_port { static constexpr uintptr_t value = T::port; };
    template <class T> struct DP_Ddr { static constexpr uintptr_t value = T::ddr; };

    template <uintptr_t A>
    using MV = typename PIN::template memvar_t<A>;

    template <typename T> struct BB_set {
    static FORCEINLINE void exec() { T::set(); }
    };

    template <typename T> struct BB_clear {
    static FORCEINLINE void exec() { T::clear(); }
    };

    template <typename T> struct BB_toggle {
    static FORCEINLINE void exec() { T::toggle(); }
    };

    // extract the n-th argument from the parameter pack
    // we use a fold expression with '|'
    template <uint8_t I, uint8_t...Is, typename...BoolArgs>
    static FORCEINLINE bool get_nth_argument(const byte_sequence<Is...>&&, const BoolArgs...args)
    {
        return ((Is == I ? args : 0) | ...);
    }

    // set values for one single port processing one PortInfo
    template <typename PORT, uint8_t MASK, uint8_t...ArgIndexes, typename...BoolArgs>
    static FORCEINLINE void set_values_1port_helper(const multi_pin_h::PortInfo<PORT, MASK, ArgIndexes...>&&, const BoolArgs...args)
    {
        static constexpr uintptr_t address = PORT::port;
        typedef ByteBits<MV<address>, MASK> byte_t;
        // we need to extract the relevant argument ordering per bit position, as expected by ByteBits
        byte_t::set_values(get_nth_argument<ArgIndexes>(make_byte_sequence<sizeof...(BoolArgs)>{}, args...)...);
    }

    // set values port by port, going sequentially thorugh all PortInfo using a fold expression on ','
    template <typename...PINFOs, typename... BoolArgs>
    static void FORCEINLINE set_values_helper(const multi_pin_h::PortInfoPack<PINFOs...>&&, const BoolArgs...args)
    {
        (set_values_1port_helper(PINFOs{}, args...),...);
    }


public:
    using portinfo_t = typename multi_pin_h::MakePortInfoPacks<0,multi_pin_h::PortInfoPack<>,PIN,PINs...>::type_t;
    using helper_t = multi_pin_h::MPHelper<portinfo_t, MV>;

    using mask_tuple_t = typename multi_pin_h::MakeTupleType<portinfo_t::nPorts>::type_t;
    static const uint8_t nPins = 1 + sizeof...(PINs);

    static FORCEINLINE void set_high()
    {
        using namespace multi_pin_h;
        helper_t::template exec<DP_port,BB_set>();
    }

    static FORCEINLINE void set_low()
    {
        using namespace multi_pin_h;
        helper_t::template exec<DP_port,BB_clear>();
    }
    static FORCEINLINE void toggle()
    {
        using namespace multi_pin_h;
        helper_t::template exec<DP_port,BB_toggle>();
    }

    static FORCEINLINE void set_value(bool high)
    {
        if (high)
            set_high();
        else
            set_low();
    }


    // the argument order is the same as the template arguments
    template <typename... BoolArgs>
    static FORCEINLINE void set_values(BoolArgs... args)
    {
        static_assert(sizeof...(args) == nPins,
            "The number of arguments does not match the number of pins");
        set_values_helper(portinfo_t{}, args...);
    }

    static FORCEINLINE void set_output()
    {
        using namespace multi_pin_h;
        helper_t::template exec<DP_Ddr,BB_set>();
    }
    static FORCEINLINE void set_input()
    {
        using namespace multi_pin_h;
        helper_t::template exec<DP_Ddr,BB_clear>();
    }
/*
    static FORCEINLINE bool read()
    {
        return pin_bits_t::get();
    }
*/

};
