#pragma once
#include <avr/io.h>
#include "macros.h"
#include "bit_utils.h"

#ifndef F_CPU
#error F_CPU undefined
#endif


struct adc
{
#if defined(__AVR_ATtiny84A__)
    enum VoltageReference { AVcc=0b00 /* 5V */, AREF=0b01, V1_1=0b10 /* 1.1V */ };
#elif  defined(__AVR_ATmega328P__) || defined(__AVR_ATmega328__)
    enum VoltageReference { AREF=0b00, AVcc=0b01 /* 5V */, V1_1=0b11 /* 1.1V */ };
#endif

    enum Channel  { Ch0, Ch1, Ch2, Ch3, Ch4, Ch5, Ch6, Ch7, AGND = 0b100000, IREF1_1 = 0b100001, ChTemp = 0b100010/* for temperature */ };
    enum Prescaler { Div2=0b000, Div4=0b010, Div8=0b010, Div16=0b100, Div32=0b101, Div64=0b110, Div128=0b111};

    // true if a conversion is in progress
    static FORCEINLINE bool busy()
    {
        return ADCSRA & bit8(ADSC);
    }

    // set the divisor to 128 (nned to achieve 50-200 khz
    static FORCEINLINE void set_prescaler(Prescaler p)
    {
        ADCSRA = (ADCSRA & ~0b111) | p;
    }


    // start a conversion
    static FORCEINLINE void trigger()
    {
        ADCSRA |= bit8(ADSC);
    }

    // enable the adc and set the divisor to 128
    // which is a good value given the clock at 16Ghz
    static FORCEINLINE void enable()
    {
        ADCSRA |= bit8(ADEN);
    }

    // disable the ADC
    static FORCEINLINE void disable()
    {
        ADCSRA &= ~bit8(ADEN);
    }

    // init the adc setting voltage reference, adjustment and channel
    static FORCEINLINE void switch_channel(Channel ch)
    {
        ADMUX = (ADMUX & ~0b111) | ch;
    }


    // init the adc setting voltage reference, adjustment and channel
    static FORCEINLINE void init_admux( VoltageReference vr, Channel ch,
                        bool leftAdjust )
    {
#if defined(__AVR_ATtiny84A__)
    ADMUX = (vr << REFS0) | ch;
    if(leftAdjust)
        ADCSRB |= bit8(ADLAR);
#elif  defined(__AVR_ATmega328P__) || defined(__AVR_ATmega328__)
    ADMUX = (vr << REFS0) | (leftAdjust? bit8(ADLAR):0) | ch;
#endif
    }

    // connect input to 1.1V and set AREF to 1.1V:
    // reading will always return 1023
    static FORCEINLINE void init_admux_100( bool leftAdjust )
    {
        ADMUX = (AVcc<<REFS0) | (leftAdjust? (1<<ADLAR):0) | 0xA;
    }

    // connect input to GND:
    // reading will always return 0
    static FORCEINLINE void init_admux_0( bool leftAdjust )
    {
        ADMUX = (V1_1 <<REFS0) | (leftAdjust? (1<<ADLAR):0) | 0xF;
    }

    static FORCEINLINE uint16_t readFull()
    {
        while (busy());
        return ADCL | (ADCH << 8);
    }

    static FORCEINLINE uint8_t readLeft()
    {
        while (busy());
        return ADCH;
    }

    static FORCEINLINE uint16_t triggerReadFull()
    {
        trigger();
        return readFull();
    }

    static FORCEINLINE uint8_t triggerReadLeft()
    {
        trigger();
        return readLeft();
    }

};
