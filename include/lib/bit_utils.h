#pragma once

#include <stddef.h>
#include <stdint.h>

#include "macros.h"
#include "assert_util.h"
#include <type_traits>
#include <utility>

template <uint8_t...Is>
using byte_sequence = std::integer_sequence<uint8_t, Is...>;

template <uint8_t N>
using make_byte_sequence = std::make_integer_sequence<uint8_t, N>;

namespace details {

// shift left a boolean value by by N
template <typename MaskType, uint8_t N>
inline constexpr MaskType shiftbool(bool v)
{
    return static_cast<MaskType>(v) << N;
}

// returns a mask with only the bit in position bitindex set
template <typename MaskType>
inline constexpr MaskType bitmask(uint8_t bitindex)
{
    return static_cast<MaskType>(0x1) << bitindex;
}

template <uint8_t SearchPos, class MaskType>
inline constexpr uint8_t bit_pos_in_mask(MaskType m, uint8_t index)
{
    return (SearchPos >= 8*sizeof(MaskType))
        ? 0xFF
        : (m & (bitmask<MaskType>(SearchPos)))
            ? (index == 0)
                ? SearchPos
                : bit_pos_in_mask<SearchPos+1>(m, index-1)
            : bit_pos_in_mask<SearchPos+1>(m, index);
}

template <typename MaskType, uint8_t...Is>
inline constexpr uint8_t n_active_bits_aux(MaskType m, byte_sequence<Is...>&&)
{
    static_assert(sizeof...(Is) == sizeof(MaskType) * 8);
    // accumulates 1 or 0 depending if the expression is set or not
    return (... + static_cast<bool>(bitmask<MaskType>(Is) & m));
}

// convert all arguments to boolean and packs result in a single integer of type MaskType
template <typename MaskType, typename...Args, uint8_t...Is>
inline constexpr MaskType pack_args(Args...args, byte_sequence<Is...>&&)
{
    return (static_cast<MaskType>(0) | ... | (static_cast<bool>(args) << Is));
}

template <typename MaskType, typename...Args, uint8_t...Is>
inline constexpr MaskType pack_args(byte_sequence<Is...>&&, Args...args)
{
    return (MaskType(0) | ... | MaskType((args?1:0) << Is));
}


// construct a byte sequence containnig the indices of the active bits in the MASK
template <typename T, T M, uint8_t I, typename S>
struct BitMaskSequenceImpl;

template <typename T, T MASK, uint8_t...Is>
struct BitMaskSequenceImpl<T, MASK, 0, byte_sequence<Is...>>
{
    using type = byte_sequence<Is...>;
};

template <typename T, T MASK, uint8_t I, uint8_t...Is>
struct BitMaskSequenceImpl<T, MASK, I, byte_sequence<Is...>>
{
    using seq  = typename std::conditional<MASK & (1 << (I-1)), byte_sequence<I-1, Is...>, byte_sequence<Is...>>::type;
    using type = typename BitMaskSequenceImpl<T, MASK, I-1, seq>::type;
};


}; // namespace details

// The following 3 utilities return a bitmask of type uint8_t, uint16_t, uint32_t with
// one single active bit in position bitindex
constexpr auto bit8 = details::bitmask<uint8_t>;
constexpr auto bit16 = details::bitmask<uint16_t>;
constexpr auto bit32 = details::bitmask<uint32_t>;

// The following 3 utilities return a bitmask of type uint8_t, uint16_t, uint32_t with
// one single active bit in position bitindex
template <uint8_t N>
constexpr auto shiftbool8 = details::shiftbool<uint8_t, N>;
template <uint8_t N>
constexpr auto shiftbool16 = details::shiftbool<uint16_t, N>;
template <uint8_t N>
constexpr auto shiftbool32 = details::shiftbool<uint32_t, N>;

// count the number of active bits in a mask
template <class MaskType>
inline constexpr FORCEINLINE uint8_t n_active_bits(MaskType mask)
{
    return details::n_active_bits_aux(mask, make_byte_sequence<sizeof(mask) * 8>{});
    //mask == 0 ? 0 : 1 + n_active_bits(mask & (mask-1));
}

// compute the position of the n_th-th active bit in the mask
// for instance, in the mask 0b00010001, there are one bit in position
// 0 and one bit in position 5, therefore if n_th==0 then
// pos=0, if n_th==2 then pos=5
template <class MaskType>
inline constexpr uint8_t bit_index(MaskType mask, uint8_t n_th)
{
    return details::bit_pos_in_mask<0>(mask, n_th);
}

// smallest uint type that can range from 0 to N-1
template <uint32_t N>
using smallest_uint_t = typename std::conditional<(N <= 0xFFul+1), uint8_t, typename std::conditional<(N <= 0xFFFFul+1), uint16_t, uint32_t>::type>::type;

// number of bytes necessary to contain n bits
inline constexpr uint32_t calc_n_bytes(uint32_t n) { return n / 8 + (((n % 8) > 0) ? 1 : 0); }

// convert all arguments to boolean and packs result in a single integer of type MaskType
template <typename MaskType, typename...Args>
inline FORCEINLINE constexpr MaskType pack_args(Args...args)
{
    static_assert(sizeof(MaskType) * 8 == sizeof...(Args));
    return details::pack_args<MaskType>(make_byte_sequence<8 * sizeof(MaskType)>{},static_cast<bool>(args)...);
}

// define a integer sequence type containing the indices of the bits active in the mask
// e.g. if MASK=0b10001001, it defines the type integer_sequence<uint8_t, 0, 3, 7>
// because the active bits are in position 0, 3 and 7
template <typename T, T MASK>
using bitmask_sequence = typename details::BitMaskSequenceImpl<T, MASK, 8*sizeof(T), byte_sequence<>>::type;
template <uint8_t MASK>
using bitmask8_sequence = bitmask_sequence<uint8_t, MASK>;

constexpr uint8_t reverse_bits(uint8_t data)
{
    // smallest code we can get (tested with godbolt)
    uint8_t b = 0;
    if (data & bit8(0)) b |= bit8(7);
    if (data & bit8(1)) b |= bit8(6);
    if (data & bit8(2)) b |= bit8(5);
    if (data & bit8(3)) b |= bit8(4);
    if (data & bit8(4)) b |= bit8(3);
    if (data & bit8(5)) b |= bit8(2);
    if (data & bit8(6)) b |= bit8(1);
    if (data & bit8(7)) b |= bit8(0);
    return b;
}

/*
 * TESTS
*/

// test bit masks

namespace {

    using namespace assert_util_h;

    template <typename F, uint8_t...Is>
    constexpr bool test_bitmask(F f, byte_sequence<Is...>&&)
    {
        static_assert(sizeof...(Is) == 8 * sizeof(decltype(f(0))));
        constexpr uint8_t nbits = 8 * sizeof(decltype(f(0)));
        return (true && ... && (f(Is) == (1ul << Is)));
    }

    // test bit8, bit16 and bit32 (all valid combinations)
    static_assert(test_bitmask(bit8, make_byte_sequence<8>{}), "Test bit8 failed" );
    static_assert(test_bitmask(bit16, make_byte_sequence<16>{}), "Test bit16 failed" );
    static_assert(test_bitmask(bit32, make_byte_sequence<32>{}), "Test bit32 failed" );


    // test n_active_bits
    static_assert(n_active_bits(0x01) == 1, "Test n_active_bits failed");
    static_assert(n_active_bits(0x02) == 1, "Test n_active_bits failed");
    static_assert(n_active_bits(0x04) == 1, "Test n_active_bits failed");
    static_assert(n_active_bits(0x08) == 1, "Test n_active_bits failed");
    static_assert(n_active_bits(0x10) == 1, "Test n_active_bits failed");
    static_assert(n_active_bits(0x20) == 1, "Test n_active_bits failed");
    static_assert(n_active_bits(0x40) == 1, "Test n_active_bits failed");
    static_assert(n_active_bits(0x80) == 1, "Test n_active_bits failed");
    static_assert(n_active_bits(0x81) == 2, "Test n_active_bits failed");
    static_assert(n_active_bits(0xFF) == 8, "Test n_active_bits failed");
    static_assert(n_active_bits(0x00) == 0, "Test n_active_bits failed");

    static_assert(n_active_bits(0x0000) == 0, "Test n_active_bits failed");
    static_assert(n_active_bits(0x00FF) == 8, "Test n_active_bits failed");
    static_assert(n_active_bits(0xFF00) == 8, "Test n_active_bits failed");

    static_assert(n_active_bits(0x00000000) == 0, "Test n_active_bits failed");
    static_assert(n_active_bits(0x000000FF) == 8, "Test n_active_bits failed");
    static_assert(n_active_bits(0x0000FF00) == 8, "Test n_active_bits failed");
    static_assert(n_active_bits(0x00FF0000) == 8, "Test n_active_bits failed");
    static_assert(n_active_bits(0xFF000000) == 8, "Test n_active_bits failed");
    static_assert(n_active_bits(0xFFFFFFFF) == 32, "Test n_active_bits failed");

    // tests for bit_index
    static_assert(bit_index(0x11,0) == 0, "Test bit_index error");
    static_assert(bit_index(0x11,1) == 4, "Test bit_index error");
    static_assert(bit_index(0x91,1) == 4, "Test bit_index error");
    static_assert(bit_index(0x91,2) == 7, "Test bit_index error");
    static_assert(bit_index(0x91,3) == 0xFF, "Test bit_index error");

    // check calc_n_bytes calculation
    static_assert(calc_n_bytes(0) == 0);
    static_assert(calc_n_bytes(1) == 1);
    static_assert(calc_n_bytes(8) == 1);
    static_assert(calc_n_bytes(9) == 2);
    static_assert(calc_n_bytes(16) == 2);
    static_assert(calc_n_bytes(17) == 3);
    static_assert(calc_n_bytes(256) == 32);
    static_assert(calc_n_bytes(257) == 33);

    // check length_t definition
    static_assert(std::is_same<smallest_uint_t<0xFFul+1>,uint8_t>::value);
    static_assert(std::is_same<smallest_uint_t<0xFFul+2>,uint16_t>::value);
    static_assert(std::is_same<smallest_uint_t<0xFFFFul+1>,uint16_t>::value);
    static_assert(std::is_same<smallest_uint_t<0xFFFFul+2>,uint32_t>::value);

    // check pack_args
    static_assert(pack_args<uint8_t>(0,1,1,0,0,1,0,1) == 0b10100110);

    // bit_mask_sequence
    static_assert(CheckSame<bitmask8_sequence<0b10100101>, byte_sequence<0, 2, 5, 7>>::value);

    // reverse_bits
    static_assert(reverse_bits(0b0) == 0);
    static_assert(reverse_bits(bit8(0)) == bit8(7));
    static_assert(reverse_bits(bit8(1)) == bit8(6));
    static_assert(reverse_bits(0xFF) == 0xFF);
    static_assert(reverse_bits(0b11110000) == 0b1111);
    static_assert(reverse_bits(0b1111) == 0b11110000);
    static_assert(reverse_bits(0b11010100) == 0b00101011);
};  // anonymous namespace
