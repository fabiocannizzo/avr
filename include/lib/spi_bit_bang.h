#pragma once

#include "macros.h"
#include "bit_utils.h"

#include <utility>

// Possible addition to make the code faster
// - send_zeroes: send zeros to all pins
// - send byte with mask: bits not present in the mask are sent with unspecified values

template < typename CLK
         , typename DIN
         , typename LOAD
         , bool txFast = true    // true cause faster transmission, but slightly larger source code
         , bool txWhenLoadLow = true // if true, start transmission with LOAD=LOW and latch on LOAD=HIGH, otherwise viveversa
         , bool txOnRaisingClk = true // if true, performs single bit transmission on raising clock
         >
struct BitBang
{
    static_assert(CLK::nPins == 1, "CLK must be one single pin");
    static_assert(LOAD::nPins == 1, "LOAD must be one single pin");

    static constexpr uint8_t nPins = DIN::nPins;
    static constexpr uint8_t nChains = nPins;
    static constexpr bool use_parallel_chains = true; // send to multiple chains in parallel

private:

    static void tx_bit_start()
    {
        if (txOnRaisingClk)
            CLK::set_low();
        else
            CLK::set_high();
    }

    static void tx_bit_end()
    {
        if (txOnRaisingClk)
            CLK::set_high();
        else
            CLK::set_low();
    }


    // either send to all pins the bit in byte specified by mask,
    // or send to each bin the bit in byte from the correspondent element of the tuple (byte,args...)
    template <typename...BoolArgs>
    FORCEINLINE static void send_bits(const uint8_t mask, uint8_t byte, const BoolArgs...bytes)
    {
        tx_bit_start();
        if constexpr(sizeof...(bytes))
            DIN::set_values(byte & mask, (bytes & mask)...);
        else
            DIN::set_value(byte & mask);
        tx_bit_end();
    }

    // send to each pin whatever the current value of the respective elements of DIN are
    FORCEINLINE static void send_current_DIN_value()
    {
        tx_bit_start();
        tx_bit_end();
    }


    // send one byte per pin: arg0 to pin0, arg1 to pin1, etc...
    template <bool MSB_FIRST, typename...Bytes>
    static void send_bytes_impl(const Bytes...args)
    {
        if constexpr(txFast) {
            send_bits(bit8(MSB_FIRST ? 7 : 0), args...);
            send_bits(bit8(MSB_FIRST ? 6 : 1), args...);
            send_bits(bit8(MSB_FIRST ? 5 : 2), args...);
            send_bits(bit8(MSB_FIRST ? 4 : 3), args...);
            send_bits(bit8(MSB_FIRST ? 3 : 4), args...);
            send_bits(bit8(MSB_FIRST ? 2 : 5), args...);
            send_bits(bit8(MSB_FIRST ? 1 : 6), args...);
            send_bits(bit8(MSB_FIRST ? 0 : 7), args...);
        }
        else {
            if constexpr(MSB_FIRST) {
                uint8_t mask = 0x80;
                do {
                    send_bits(mask, args...);
                } while((mask >>= 1) != 0);
            }
            else {
                uint8_t mask = 0x01;
                do {
                    send_bits(mask, args...);
                } while((mask <<= 1) != 0);
            }
        }
    }


public:
    static void start()
    {
        if (txWhenLoadLow)
            LOAD::set_low();
        else
            LOAD::set_high();
    }

    static void end()
    {
        if (txWhenLoadLow)
            LOAD::set_high();
        else
            LOAD::set_low();
    }

    // send one byte per pin: arg0 to pin0, arg1 to pin1, etc...
    template <bool MSB_FIRST, typename...Bytes>
    static void send_bytes(const Bytes...args)
    {
        static_assert(nPins == sizeof...(Bytes), "Number of bytes does not match number of pins");
        send_bytes_impl<MSB_FIRST>(args...);
    }

    // send the same byte to all pins
    template <bool MSB_FIRST>
    static void send_same_byte(const uint8_t byte)
    {
        send_bytes_impl<MSB_FIRST>(byte);
    }

    // send zero to all pins
    static void send_zero()
    {
        send_bits(0, 0);
        for (uint8_t i = 0; i < 6; ++i)
            send_current_DIN_value();
    }

    static void init()
    {
        CLK::set_output();
        DIN::set_output();
        LOAD::set_output();
        CLK::set_high();
        end();
    }
};


