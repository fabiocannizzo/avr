#pragma once

#include "macros.h"
#include "bit_utils.h"

#include <string.h>
#include <utility>
#include <algorithm>


template < typename TX
         , uint8_t nDigitPerDevice
         , uint8_t nDevicePerChain
         , bool reverseDevices  // reverse the order of the devices in each chain
         , bool reverseDigits   // reverse the order of the digits within each device
         , bool reverseSegments // reverse the order of segments within each digit
         >
struct max7219
{
    typedef max7219<TX, nDigitPerDevice, nDevicePerChain, reverseDevices, reverseDigits, reverseSegments> led_driver_t;
private:
    static_assert(nDigitPerDevice <= 8, "MAX7219 supports a maximum of 8 digits");

    constexpr static bool isBitBang = true;

    enum SpecialControlBytes
        { NOOP       = 0x00
        , DECODE     = 0x09
        , INTENSITY  = 0x0A
        , SCAN_LIMIT = 0x0B
        , POWER      = 0x0C
        , TEST       = 0x0F
        };

    static constexpr uint8_t nChains = TX::nChains;

    static constexpr uint8_t devByteIndex(uint8_t dev)
    {
        return (reverseDevices ? (nDevicePerChain - 1 - dev) : dev);
    }

public:

    typedef uint8_t data_buffer_t[nDigitPerDevice][nDevicePerChain][nChains];

    static uint8_t& get_byte(data_buffer_t data, uint8_t dig, uint8_t dev, uint8_t ch) { return data[dig][dev][ch]; }
    static const uint8_t& get_byte(const data_buffer_t data, uint8_t dig, uint8_t dev, uint8_t ch) { return data[dig][dev][ch]; }


    template<bool VerticalChains, bool DigitsAreColumns>
    struct Matrix
    {
        static const uint8_t nRowsPerDev =  DigitsAreColumns ? 8 : nDigitPerDevice;
        static const uint8_t nColsPerDev = !DigitsAreColumns ? 8 : nDigitPerDevice;
        static const uint8_t nDevsPerCol =  VerticalChains ? nDevicePerChain : TX::nPins;
        static const uint8_t nDevsPerRow = !VerticalChains ? nDevicePerChain : TX::nPins;
        static const uint8_t nRows = nRowsPerDev * nDevsPerRow;
        static const uint8_t nCols = nColsPerDev * nDevsPerCol;

    private:
        data_buffer_t data;

        uint8_t get_chain(uint8_t r, uint8_t c) const
        {
            return !VerticalChains ? r / nRowsPerDev : c / nColsPerDev;
        }
        uint8_t get_dev(uint8_t r, uint8_t c) const
        {
            return  VerticalChains ? r / nRowsPerDev : c / nColsPerDev;
        }
        uint8_t get_digit(uint8_t r, uint8_t c) const
        {
            return (DigitsAreColumns ? c : r) % nDigitPerDevice;
        }

    public:

        static const size_t buffer_size = sizeof(data);

        operator data_buffer_t&() { return data; }
        operator const data_buffer_t&() const { return data; }

        uint8_t& get_byte_ref(uint8_t r, uint8_t c)
        {
            return led_driver_t::get_byte(data, get_digit(r, c), get_dev(r, c), get_chain(r, c));
        }

        const uint8_t& get_byte_ref(uint8_t r, uint8_t c) const
        {
            return led_driver_t::get_byte(data, get_digit(r, c), get_dev(r, c), get_chain(r, c));
        }

        static uint8_t get_mask(uint8_t r, uint8_t c)
        {
            return bit8((!DigitsAreColumns ? c : r) % nDigitPerDevice);
        }

        void refresh()
        {
            led_driver_t::send_data(data);
        }

        void set_all(bool on, bool send = false)
        {
            const uint8_t mask = on ? 0xFF : 0;
            memset(&data[0][0][0], mask, sizeof(data));
            if (send)
                led_driver_t::set_all(mask);
        }

        bool set_led(uint8_t r, uint8_t c, bool value, bool send = false)
        {
            uint8_t& b = get_byte_ref(r, c);
            const uint8_t mask = get_mask(r, c);
            bool ret = b & mask;
            if(value)
                b |= mask;
            else
                b &= ~mask;
            if (send) {
                const uint8_t dig   = (DigitsAreColumns ? c : r) % nDigitPerDevice;
                led_driver_t::send_data(data, bit8(dig));
            }
            return ret;
        }

        bool get_led(uint8_t r, uint8_t c) const
        {
            const uint8_t& b = get_byte_ref(r, c);
            const uint8_t mask = get_mask(r, c);
            return b & mask;
        }

        uint8_t *getBuffer() { return &data[0][0][0]; }
        const uint8_t *getBuffer() const { return &data[0][0][0]; }

    };


    static void send_control(uint8_t ctrl, uint8_t value)
    {
        if constexpr(TX::use_parallel_chains) {
            TX::start();
            for (uint8_t dev = 0; dev < nDevicePerChain; ++dev) {
                TX::template send_same_byte<true>(ctrl);
                TX::template send_same_byte<true>(value);
            }
            TX::end();
        }
        else {
            // FIXME: untested
            for(uint8_t ch = 0; ch < nChains; ++ch) {
                TX::start(ch);
                for (uint8_t dev = 0; dev < nDevicePerChain; ++dev) {
                    TX::template send_byte<true>(ctrl);
                    TX::template send_byte<true>(value);
                }
                TX::end(ch);
            }
        }
    }

private:
    template <uint8_t...Chs>
    FORCEINLINE static void send_data_impl(const uint8_t data[nChains], const byte_sequence<Chs...>&&)
    {
        TX::template send_bytes<!reverseSegments>((data[Chs])...);
    }

public:

    // Send data correspondent to the d-th digit in a 7 segment display
    // or the data for a matrix row or column
    // For a 7 segment display this could be either as a bitmask or encoded,
    // while for a matrix row (or column) this is a bitmask
    static void send_data(const data_buffer_t& data, uint8_t which_mask = 0xFF)
    {
        for (uint8_t digit = 0; digit < nDigitPerDevice; ++digit) {
            const uint8_t idig = reverseDigits ? (nDigitPerDevice - 1) - digit : digit;
            if (!(which_mask & bit8(idig)))
                continue;
            if constexpr(TX::use_parallel_chains) {
                TX::start();
                for (uint8_t dev = 0; dev < nDevicePerChain; ++dev) {
                    const uint8_t idev = devByteIndex(dev);
                    // send target digit
                    TX::template send_same_byte<true>(digit + 1);
                    // send digit values per each chain
                    send_data_impl(data[idig][idev], make_byte_sequence<nChains>{});
                }
                TX::end();
            }
            else {
                // FIXME: untested
                static_assert(!reverseSegments, "not implemented");
                for(uint8_t ch = 0; ch < nChains; ++ch) {
                    TX::start(ch);
                    for (uint8_t dev = 0; dev < nDevicePerChain; ++dev) {
                        TX::send_byte(digit + 1);
                        TX::send_byte(data[ch][devByteIndex(dev)][idig]);
                    }
                    TX::end(ch);
                }
            }
        }
    }


    // set power mode (shutdown) on/off
    static void set_power(bool on)
    {
        send_control(POWER, on);
    }

    // set test mode on/off
    static void set_test_mode(bool on)
    {
        send_control(TEST, on);
    }

    // level must be in the range (0, 15)
    // it uses PWM wih duty cycle is: (2 * level + 1) / 32
    static void set_intensity(uint8_t level)
    {
        send_control(INTENSITY, level);
    }

    // set decode mode
    // which_bitmask defines which digits are intended to use decoding
    static void set_decode_mode(uint8_t which_bitmask = 0)
    {
        send_control(DECODE, which_bitmask);
    }

    // Set the number of digits in use, for a 7 segment display,
    // or the number of rows (or columns) in use, for a matrix of leds.
    // N must be in the range (0,7)
    // Note that this has an impact on dimness, hence it is better not to change it after initialization
    static void set_scan_limit(uint8_t n)
    {
        send_control(SCAN_LIMIT, n);
    }

    static void set_all(uint8_t mask)
    {
        for (uint8_t dig = 0; dig < nDigitPerDevice; ++dig)
            send_control(dig + 1, mask);
    }

    static void clear()
    {
        set_all(0);
    }

    // FIXME: add configuration arguments to this function
    static void init()
    {
        // init trasmission pins
        TX::init();

        // Decode mode: none
        set_decode_mode();

        // level must be in the range (1,31)
        set_intensity(31);

        // Scan limit: All "digits" (rows) on
        set_scan_limit(nDigitPerDevice - 1);

        // Shutdown register: Display on
        set_power(true);

        // Display test: off
        set_test_mode(false);

        // clear
        clear();
    }


};
