#pragma once

#include "macros.h"
#include "multi_pin.h"
#include "bit_array.h"

#include <utility>

namespace _74hc595_h_ {
template <typename T> struct SinglePinIfDefined { static constexpr bool value = T::nPins == 1; };
template <> struct SinglePinIfDefined<void> { static constexpr bool value = true; };
}

/*
 Allow to control one or multiple 74HC595 chips, eiher daisy chained
 and driven from the same data pin or driven by multiple data pins,
 or a combination of the two.
 When multiple data pins (chains) are used, it is more efficient if they are
 on the same port.
 With multiple pis (chains), the number of devices daisy-chained in each chain
 must be the same.
 Latch and shift must use a single pin for all chains

 The data structure used is hc<>::buffer_t, which is of type bit_array<NBITS>[NCHAINS]
 In terms of electrical connections, if revert=true, the first bit in every array
 correspond to the first pin in the correspondent physical chain
 (i.e. pin0 of the first device in the chain)

 Example:
    buffer_t hc<...>::data;  // declaration
    data[2].get(6); // returns the 7-th bit associated with the 3-rd chain
    data[1].set(2, 1); // set to high the 3-rd bit associated with the 2-nd chain

    buffer_t hc<...>::data{{0xFF, 0xA4}, {0X00, 0x32}}; // declaration and initialization (2 chains of up to 16-bits each)
*/
// FIXME: add reset pin and OE pin (note this can also be connected to a PWM pin causing all output to do PWM))
template < typename ST_CP // storage pin (can be the same as the shift pin)
     , typename SH_CP // shift pin
     , typename DS    // data pins: can be a Pin or a MultiPin
     , uint16_t NBITS // nbits per each data pin
     , bool reverse  // revert pin order within a chain
     , typename OE = void // enable/disable pin
     , typename MR = void // reset pin
     >
struct hc595
{
    using nbit_t = smallest_uint_t<NBITS>;

    static constexpr uint8_t nChains = DS::nPins;
    static constexpr nbit_t nBits = static_cast<nbit_t>(NBITS);
    static constexpr bool shift_latch_same_pin = std::is_same<ST_CP,SH_CP>::value;
    static constexpr bool has_enable_pin = !std::is_same<OE,void>::value;
    static constexpr bool has_reset_pin = !std::is_same<MR,void>::value;

    typedef bit_array<nBits> buffer_t[nChains];

private:

    // static_assert(nChains <= 8, "DS cannot have more than 8 pins");
    static_assert(SH_CP::nPins == 1, "SH must be one single pin");
    static_assert(ST_CP::nPins == 1, "ST must be one single pin");
    static_assert(_74hc595_h_::SinglePinIfDefined<OE>::value, "OE must be one single pin");
    static_assert(_74hc595_h_::SinglePinIfDefined<MR>::value, "MR must be one single pin");

    static FORCEINLINE void latch()
    {
        ST_CP::set_high();
        ST_CP::set_low();
    }

    static FORCEINLINE void shift()
    {
        // if SH_CP == ST_CP, then it does shift and latch simultaneously
        SH_CP::set_high();
        SH_CP::set_low();
    }

    // load the output into the shift register and latch
    template <uint8_t...Is>
    static void output(const buffer_t& buffer, byte_sequence<Is...>&&)
    {
        // it does not make sense to load the entire sequence latching at every bit
        static_assert(!shift_latch_same_pin);

        if constexpr(reverse)
            for (nbit_t b = nBits; b-- > 0; ) {
                DS::set_values((buffer[Is].get(b))...);
                shift();
            }
        else
            for (nbit_t b = 0; b < nBits; ++b) {
                DS::set_values((buffer[Is].get(b))...);
                shift();
            }
        latch();
    }

public:

    // if enable is defined, by default it is false
    static void init()
    {
        DS::set_output();
        MultiPin<ST_CP,SH_CP>::set_output();
        MultiPin<ST_CP,SH_CP>::set_low();
        if constexpr(has_enable_pin) {
            OE::set_output();
            enable_output(false);
        }
        if constexpr(has_reset_pin) {
            MR::set_output();
            MR::set_high();
        }
    }

    // load output and latch
    template <uint8_t...Is>
    static void output(const buffer_t& buffer)
    {
        output(buffer, make_byte_sequence<nChains>{});
    }

    // enable or disable all outputs
    static void enable_output(bool enable)
    {
        static_assert(has_enable_pin);
        OE::set_value(enable);
    }
};

