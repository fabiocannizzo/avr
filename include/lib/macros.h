#pragma once

#ifndef DEBUG
#   define FORCEINLINE __attribute__((always_inline))
#else
#   define FORCEINLINE
#endif

#define STATIC_ASSERT(msg,cond) char msg[(cond)?0:-1];
