#pragma once

#include <type_traits>

namespace assert_util_h {

template <typename T1, typename T2, bool OK>
struct Incomplete;

template <typename T1, typename T2>
struct Incomplete<T1, T2, true>
{
        static constexpr bool value = true;
};

// we use this auxiliary class so that T1 and T2 show up in the error message
template <typename T1, typename T2>
struct CheckSame : Incomplete<T1,T2,std::is_same<T1,T2>::value> {};

};
