#pragma once

// get n-th type in a pack
template <size_t i, typename A, typename...As>
struct nth_type
{
    using type = typename nth_type<i - 1, As...>::type;
};

template <typename A, typename...As>
struct nth_type<0, A, As...>
{
    using type = A;
};

