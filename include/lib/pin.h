#pragma once

#include "macros.h"
#include "bytebits.h"
#include "mem_var.h"

template <typename P, uint8_t BitIndex,
          template<uintptr_t Address> class MEMVAR = MemVar8>
struct Pin
{
    static_assert(BitIndex < 8, "BitIndex exceeds port size");

    using port_t = P;

    template<uintptr_t A>
    using memvar_t = MEMVAR<A>;

    static constexpr uint8_t index = BitIndex;
    static constexpr uint8_t mask = bit8(BitIndex);
    static constexpr uint8_t nPins = 1;

    typedef ByteBits<MEMVAR<P::port>,mask> port_bits_t;
    typedef ByteBits<MEMVAR<P::ddr>,mask> ddr_bits_t;
    typedef ByteBits<MEMVAR<P::pin>,mask> pin_bits_t;

    static FORCEINLINE void set_high()
    {
        port_bits_t::set();
    }
    static FORCEINLINE void set_low()
    {
        port_bits_t::clear();
    }
    static FORCEINLINE void toggle()
    {
        port_bits_t::toggle();
    }
    static FORCEINLINE bool get_value()
    {
        return port_bits_t::get();
    }
    template <typename B>
    static FORCEINLINE void set_value(B&& high)
    {
        port_bits_t::set_value(high);
    }
    template <typename B>
    static FORCEINLINE void set_values(B&& high)
    {
        port_bits_t::set_value(high);
    }

    static FORCEINLINE void set_output()
    {
        ddr_bits_t::set();
    }
    static FORCEINLINE void set_input()
    {
        ddr_bits_t::clear();
    }
    static FORCEINLINE bool read()
    {
        return pin_bits_t::get();
    }
};
