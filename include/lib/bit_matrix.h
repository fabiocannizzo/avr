#pragma once

#include "bit_array.h"


template <uint32_t R, uint32_t C, bool RowMajor = true>
struct bit_matrix
{
    using array_t = bit_array<R * C>;
    using index_t = smallest_uint_t<(R > C) ? R : C>;
    using length_t = typename array_t::length_t;

    static constexpr index_t nRows = R;
    static constexpr index_t nCols = C;

    static constexpr length_t index(index_t i, index_t j)
    {
        return RowMajor ? i * length_t(nCols) + j : j * length_t(nRows) + i;
    }

    bit_matrix() {}
    bit_matrix(bool v) : m_data(v) {}

    bool get(index_t i, index_t j) const
    {
        return m_data.get(index(i, j));
    }

    void set(index_t i, index_t j, bool v)
    {
        m_data.set(index(i, j), v);
    }

    void set_all(bool v)
    {
        m_data.set_all(v);
    }

    uint8_t *data() { return m_data.data(); }
    const uint8_t *data() const { return m_data.data(); }

private:
    array_t m_data;
};

namespace Test {
    static_assert(bit_matrix<2,5>::index(1, 2) == 7);
    static_assert(bit_matrix<2,5,false>::index(1, 2) == 5);
}
