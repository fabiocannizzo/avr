#pragma once

#include "macros.h"
#include "bit_utils.h"
#include "assert_util.h"

#include <type_traits>

namespace bytebits_h {

template <typename IDX, uint8_t M, uint8_t I>
struct BuildBitIndex;


template <uint8_t M, uint8_t I, uint8_t...IDX>
struct BuildBitIndex<byte_sequence<IDX...>, M, I>
{
    typedef typename std::conditional<M & 1,
        byte_sequence<IDX..., I>, byte_sequence<IDX...>>::type index_t;
    typedef typename BuildBitIndex<index_t, (M >> 1), I+1>::type_t type_t;
};

template <uint8_t I, uint8_t...IDX>
struct BuildBitIndex<byte_sequence<IDX...>, 0, I>
{
    typedef byte_sequence<IDX...> type_t;
};

} // namespace bytebits_h

template <typename BYTE, uint8_t BitMask>
struct ByteBits
{
    static_assert(std::is_same<uint8_t, decltype(BYTE::get())>::value);

    static const uint8_t mask = BitMask;
    static const uint8_t nBits = n_active_bits(BitMask);

    typedef typename bytebits_h::BuildBitIndex<byte_sequence<>, mask, 0>::type_t index_t;

    typedef BYTE byte_t;

/*
    template <uint8_t H, uint8_t...T, typename... ARGS>
    static constexpr FORCEINLINE uint8_t build_mask(byte_sequence<H, T...>&&, bool head, ARGS&&... args)
    {
        return (shiftbool8<H>(head) | ... | shiftbool8<T>(args));
    }
*/

    static FORCEINLINE void set_mask(uint8_t m)
    {
        byte_t::template set_with_mask<mask>(m);
    }
    static FORCEINLINE void set()
    {
        byte_t::template set<mask>();
    }
    static FORCEINLINE void clear()
    {
        byte_t::template clear<mask>();
    }
    static FORCEINLINE void toggle()
    {
        byte_t::template toggle<mask>();
    }
    static FORCEINLINE uint8_t get()
    {
        return byte_t::get() & mask;
    }

    // the argument order must be from the lower to the higher
    // bit of the mask
    template <typename... Args>
    static FORCEINLINE void set_values(Args&&... args)
    {
        static_assert((sizeof...(Args) == nBits),
            "set_values called with incorrect number of arguments");
        byte_t::template set_with_args<mask>(args...);
    }
    static FORCEINLINE void set_value(bool high)
    {
        if (high)
            set();
        else
            clear();
    }
};

template <typename BYTE>
struct ByteBits<BYTE, 0xFF>
{
    static_assert(std::is_same<uint8_t, decltype(BYTE::get())>::value);

    static FORCEINLINE void set_mask(uint8_t m)
    {
        byte_t::set(m);
    }

    static const uint8_t mask = 0xFF;
    static const uint8_t nBits = 8;

    typedef BYTE byte_t;

    static FORCEINLINE void set()
    {
        byte_t::set(0xFF);
    }
    static FORCEINLINE void clear()
    {
        byte_t::set(0);
    }
    static FORCEINLINE uint8_t get()
    {
        return byte_t::get();
    }
    static FORCEINLINE void toggle()
    {
        byte_t::template toggle<0xFF>();
    }
    // the argument order must be from the lower to the higher
    // bit of the mask
    static FORCEINLINE void set_values(bool h0, bool h1, bool h2, bool h3, bool h4, bool h5, bool h6, bool h7)
    {
        byte_t::set(h0 | shiftbool8<1>(h1) | shiftbool8<1>(h2) | shiftbool8<3>(h3)
            | shiftbool8<4>(h4) | shiftbool8<5>(h5) | shiftbool8<6>(h6) | shiftbool8<7>(h7));
    }
    static FORCEINLINE void set_value(bool high)
    {
        if (high)
            set();
        else
            clear();
    }
};

template <typename BYTE>
struct ByteBits<BYTE,0>
{
    static const uint8_t mask = 0;
    static const uint8_t nBits = 0;

    static FORCEINLINE void set_mask(uint8_t)
    {
    }
    static FORCEINLINE void set()
    {
    }
    static FORCEINLINE void clear()
    {
    }
    static FORCEINLINE uint8_t get()
    {
        return 0;
    }
    static FORCEINLINE void set_values()
    {
    }
    static FORCEINLINE void set_value(bool high)
    {
    }
    static FORCEINLINE void toggle()
    {
    }
};

/*
namespace {

using namespace assert_util_h;

template <uint8_t T> struct X{};
struct Dummy { static uint8_t get(); };

typedef ByteBits<Dummy,0b01101110> b1_t;
static_assert(CheckSame<X<b1_t::build_mask(b1_t::index_t{},true,false,true,1,0)>,
    X<0b00101010>>::value,
    "build_mask test failed");

static_assert(CheckSame<X<b1_t::build_mask(b1_t::index_t{},1,1,1,1,1)>,
    X<0b01101110>>::value,
    "build_mask test failed");

static_assert(CheckSame<X<b1_t::build_mask(b1_t::index_t{},0,0,0,0,0)>,
    X<0b0>>::value,
    "build_mask test failed");

typedef ByteBits<Dummy,0b10101110> b2_t;
static_assert(CheckSame<X<b2_t::build_mask(b2_t::index_t{},true,false,true,0,1)>,
    X<0b10001010>>::value,
    "build_mask test failed");
} // anonmous namespace
*/
