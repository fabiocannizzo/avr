#pragma once
#include <stdint.h>

namespace hash_details
{
    template <typename UINT, uint8_t I>
    constexpr UINT pjw_mask()
    {
        const uint8_t nbytes = sizeof(UINT);
        const uint8_t nbits = 8 * nbytes;
        if constexpr(I < nbytes)
            return (UINT(1) << (nbits - 1 - I)) | pjw_mask<UINT, I+1>();
        else
            return 0;
    }
};

template <typename UINT, typename SZ>
UINT pjw_hash(const uint8_t *p, SZ n)
{
    const uint8_t nbytes = sizeof(UINT);
    const uint8_t nbits = 8 * nbytes;
    const UINT mask = hash_details::pjw_mask<UINT, 0>();

    UINT h = 0;
    for(SZ i = 0; i < n; ++i) {
        h = (h << nbytes) + p[i];
        UINT high = h & mask;
        if (high != 0) {
            h ^= (high >> (nbits * 3 / 4));
            h &= ~high;
        }
    }
    return h;
}

namespace {
    static_assert(hash_details::pjw_mask<uint8_t,0>() == 0x80);
    static_assert(hash_details::pjw_mask<uint16_t,0>() == 0xC000);
    static_assert(hash_details::pjw_mask<uint32_t,0>() == 0xF0000000);
}
