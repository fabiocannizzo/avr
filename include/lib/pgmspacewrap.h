#pragma once

#include <stdint.h>
#ifdef __AVR__
#   include <avr/pgmspace.h>
#else
#   define PROGMEM
#   define pgm_read_byte(x) (*x)
#endif
