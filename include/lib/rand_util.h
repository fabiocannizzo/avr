#pragma once

#include <stdint.h>
#include <stdlib.h>
#include <type_traits>

// Algorithm "xorwow" from p. 5 of Marsaglia, "Xorshift RNGs"
// You must provide a seed
struct XorWow
{
    typedef uint32_t rand_t;
    typedef uint32_t state_t[5];

    // The state array must be initialized to not be all zero in the first four words
    uint32_t state[5];

    XorWow() {}

    void seed(state_t newstate)
    {
        for (uint8_t i = 0; i < 5; ++i)
            state[i] = newstate[i];
    }

    template <typename T>
    using aux_t = T();

    // the argument rndBitSrc must be a function which produces
    // an integer with good entropy on the least significant bit
    template <typename T>
    void seed(const aux_t<T>& rndBitSrc)
    {
        // generate random seed and init random generator
        uint8_t *p = reinterpret_cast<uint8_t *>(state);
        for (uint8_t i = 0; i < sizeof(state); ++i) {
            uint8_t b = 1;
            for (uint8_t j = 1; j < 8; ++j) {
                uint8_t bit = rndBitSrc() & 1;
                b |= bit << j;
            }
            *p++ = b;
        }
    }

    uint32_t operator()()
    {
        uint32_t s, t = state[3];
        state[3] = state[2];
        state[2] = state[1];
        state[1] = s = state[0];
        t ^= t >> 2;
        t ^= t << 1;
        state[0] = t ^= s ^ (s << 4);
        return t + (state[4] += 362437);
    }
};

// generate uniform random numbers in the range [0...MAX]
template <uint64_t MAX, class Gen=XorWow>
struct RandGen
{
    using buffer_t = typename Gen::rand_t;
    static_assert(sizeof(buffer_t)==4);

    static constexpr uint8_t NBITS =  (MAX <= 1)   ? 1
                                    : (MAX <= 3)   ? 2
                                    : (MAX <= 7)   ? 3
                                    : (MAX <= 15)  ? 4
                                    : (MAX <= 31)  ? 5
                                    : (MAX <= 63)  ? 6
                                    : (MAX <= 255) ? 8
                                    : (MAX <= 1023) ? 10
                                    : (MAX <= 0xFFFF) ? 16
                                    : 32;

    using rand_t = typename std::conditional<(NBITS <= 8), uint8_t, typename std::conditional<(NBITS <= 16), uint16_t, uint32_t>::type>::type;

    static constexpr uint64_t domainSize = uint64_t(1) << NBITS;
    static constexpr uint8_t nRndPerDraw = sizeof(buffer_t) * 8 / NBITS;

    static constexpr rand_t mask = (rand_t)(domainSize - 1);
    static constexpr rand_t reject = (rand_t)((domainSize / (MAX + 1)) * (MAX + 1) - 1);
    static constexpr bool rejectRedundant = reject == domainSize - 1;
    static constexpr bool modRedundant = reject == MAX;

    RandGen() : m_nDrawn(nRndPerDraw - 1) {}

    rand_t operator()(Gen& gen)
    {
        rand_t rnd;
        do {
            if (++m_nDrawn == nRndPerDraw) {
                m_randBuffer = gen();
                m_nDrawn = 0;
            }
            else if(nRndPerDraw > 1) {
                m_randBuffer >>= NBITS;
            }

            rnd = static_cast<rand_t>(m_randBuffer) & mask;
        } while (!rejectRedundant && rnd > reject);
        if (!modRedundant)
            rnd %= MAX + 1;
        return rnd;
    }

private:
    buffer_t m_randBuffer;
    uint8_t m_nDrawn;
};
