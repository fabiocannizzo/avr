#pragma once
#include "bit_utils.h"

// A digital signal debouncer based on a 7-bit integrator.
// The smoothedSignal changes to true when the integrator reaches MAX, then goes back to zero when the integrator reaches 0.
// MAX must be calibrated for the smapling rate and debouncing period, with the formula:
// MAX ~ ceil(DEBOUNCE_TIME / SAMPLING_PERIOD)
// For instance, if DEBOUNCE_TIME=10ms and SAMPLIG_PERIOD=2ms, then MAX=10/2=5
template <uint8_t MAX, bool HighMeansTrue = true>
struct Debouncer
{
    static_assert(MAX <= 0b0111111);

    Debouncer(bool initValue = false)
        : state(HighMeansTrue == initValue ?  bit8(7) | MAX : 0)
    {
    }

    bool update(const bool signal)
    {
        uint8_t integrator = state & 0b01111111;
        if (signal == 0) {
            if (integrator > 1)
                state--;    // smoothedSignal unchanged, integrator--
            else
                state = 0;  // smoothedSignal = 0, integrator = 0
                return !HighMeansTrue;  // we know the smoothedSignal, hence we return it straight away
        }
        else { // signal == 1
            if (integrator < (MAX - 1))
                state++;   // smoothedSignal unchanged, integrator++
            else {
                state = bit8(7) | MAX;  // smoothedSignal = 1, integrator = MAX
                return HighMeansTrue;   // we know the smoothedSignal, hence we return it straight away
            }
        }

        return getState();
    }

    bool getState() const
    {
        uint8_t smoothedSignal = state & bit8(7);
        return HighMeansTrue ? smoothedSignal: !smoothedSignal;
    }

private:
    uint8_t state; // smoothedSignal stored in the most significant bit, integrator stored in the ramainig 7 bits
};


// A button which switch between N states
// Suppose N=3, the button switches between states {0, 1, 2}
// At the first click, it goes from 0 to 1, at the next click from 1 to 2, at the next click it goes back to 0
// A click is defined as a signal raising edge
template <uint8_t N>
struct StateSwitcher
{
    StateSwitcher(uint8_t init) : state(init) {}  // state=init, old_signal=0
    bool update(bool signal)
    {
        uint8_t curState = getState();
        if(signal) {
            const bool old_signal = state & bit8(7);
            if(!old_signal) { // raising edge detected
                if (curState < N-1)
                    ++curState;
                else
                    curState = 0;
                state = curState | bit8(7);   // toggle state and set old_signal=1
            }
        }
        else { // signal=0
            state &= ~bit8(7);  // old_signal = 0
        }
        return curState;  // since we know the state, we return it straight away
    }

    uint8_t getState()
    {
        return state & 0b01111111;
    }

private:
    uint8_t state;  // we only use two bits: 'old_signal' is bit 7 and 'currentState' is in the remaining bits
};

// specialized version of StateSwitcherButtton for N=2
template <>
struct StateSwitcher<2>
{
    StateSwitcher(uint8_t init) : state(init) {}  // state=init, old_signal=0
    bool update(bool signal)
    {
        if(signal) {
            const bool old_signal = state & bit8(7);
            if(!old_signal)  // raising edge detected
                state ^= bit8(7) | 1;        // toggle state and set old_signal=1
        }
        else { // signal=0
            state &= ~bit8(7);  // old_signal = 0
        }
        return getState();
    }
    uint8_t getState()
    {
        return state & 0x1;
    }

private:
    uint8_t state;  // we only use two bits: 'old_signal' is bit 7 and 'currentState' is in bit 0
};

enum ButtonEvent {NoEvent=0, Pending=1, Click=2, DblClick=4};

// Monitor a signal for click or double clicks.
// If a click happens, the status chamge to Pending amd we wait up to NT ticks waiting for another click.
// If a second click heppens in time, we decide it is a double-click,
// otherwise, if NT ticks pass, we decide it is a click.
// When a new event comes in, the previous event is discarded.
// NT is the double-click maximum time interval expressed in number of ticks.
// So the period is DT=NT*SamplePeriod
template <uint8_t NT>
struct Button
{
private:
    uint8_t cnt = 0;
    uint8_t state = 0;  // bit 7: old signal, bit 0 and 1: Event
    static const uint8_t mask = bit8(7);

public:
    ButtonEvent update(bool pressed)
    {
        const ButtonEvent btnState = ButtonEvent(state & ~mask);

        // check for new event and update old_state
        // discard last event
        if (btnState != Pending) {
            if(pressed) {
                const uint8_t old_signal = state & mask;
                if(!old_signal) {           // raising edge detected
                    state = Pending | mask; // pending a decision on click or double-click, old_signal=1
                    cnt = 0;                // reset timer
                    return Pending;
                }
            }
            else {  // !pressed
                state &= ~mask;  // old_signal=0
            }
            return getEvent();
        }

        // if we get here, there is a decision pending
        // decide if a decision can be made and update old_signal

        // do we have raising edge, which would imply a double click?
        if (pressed) {
            const uint8_t old_signal = state & mask;
            if (!old_signal) { // raising edge detected, it is a double-click
                state = DblClick | mask;  // // event=DblClick, old_signal=0
                return DblClick;
            }
            // if we get here, old_signal=1, i.e. there is no raising edge
            if(++cnt == NT) {  // time expired: it is a click event
                state = mask | Click;  // event=Click, old_signal=1
                return Click;
            }

            return getEvent();
        }

        // if we get here there is an event pending and the button is not pressed, so old_signal=0
        if(++cnt == NT) {  // time expired: it is a click event
            state = Click;  // event=Click, old_signal=0
            return Click;
        }

        // if we get here, there is an event pending and the old_signal=0
        state &= ~mask;  // event=Pending, old_signal=0
        return Pending;
    }

    void reset()
    {
        state &= mask;  // Event=NoEvent, old_signal unchanged
    }

    ButtonEvent getEvent() const
    {
        return ButtonEvent(state & ~mask);
    }

    ButtonEvent getEventAndReset(uint8_t eventmask)
    {
        uint8_t e = state & eventmask;
        if (e) {
            reset();
            return ButtonEvent(e);
        }
        return NoEvent;
    }

};

