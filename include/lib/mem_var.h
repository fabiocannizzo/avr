#pragma once

#include "macros.h"
#include "bit_utils.h"

template <typename T, typename C>
struct MemVarBase
{
    enum IOOpsMode {InterruptSafeOps, FastestOps, MergedOps};

private:

    static constexpr IOOpsMode defaultIOOpsMode = FastestOps;
    static constexpr T fullmask = ~T(0);

#ifdef _HAS_ATOMIC_IO_OR
    static constexpr bool has_atomic_io_or = true;
#else
    static constexpr bool has_atomic_io_or = false;
#endif

#ifdef _HAS_ATOMIC_IO_AND
    static constexpr bool has_atomic_io_and = true;
#else
    static constexpr bool has_atomic_io_and = false;
#endif

    template <uint8_t...Is>
    static FORCEINLINE void set_is(const byte_sequence<Is...>&&)
    {
        // set the bits one by one
        ((*C::ptr() |= (T(1) << Is)),...);
    }

    template <uint8_t...Is>
    static FORCEINLINE void clear_is(const byte_sequence<Is...>&&)
    {
        // clear the bits one by one
        ((*C::ptr() &= ~(T(1) << Is)),...);
    }

    template <uint8_t I>
    static FORCEINLINE void toggle_1bit_is()
    {
        constexpr T mask = 1 << I;
        if(*C::ptr() & mask)
            *C::ptr() &= ~mask;
        else
            *C::ptr() |= mask;
    }

    template <uint8_t...Is>
    static FORCEINLINE void toggle_is(const byte_sequence<Is...>&&)
    {
        // toggle the bits one by one
        (toggle_1bit_is<Is>(),...);
    }

    template <uint8_t I, typename BoolArg>
    static FORCEINLINE void set_with_args_1bit_is(const BoolArg arg)
    {
        if (arg)
            *C::ptr() |= (T(1) << I);
        else
            *C::ptr() &= ~(T(1) << I);
    }

    template <uint8_t...Is, typename...BoolArgs>
    static FORCEINLINE void set_with_args_is(const byte_sequence<Is...>&&, const BoolArgs...args)
    {
        (set_with_args_1bit_is<Is>(args),...);
    }

    template <uint8_t I>
    static FORCEINLINE void set_with_mask_1bit_is(const T mask)
    {
        if (mask & (1 << I))
            *C::ptr() |= (T(1) << I);
        else
            *C::ptr() &= ~(T(1) << I);
    }

    template <uint8_t...Is>
    static FORCEINLINE void set_with_mask_is(const byte_sequence<Is...>&&, const T mask)
    {
        (set_with_mask_1bit_is<Is>(mask),...);
    }

    template <uint8_t...Is, typename...BoolArgs>
    static FORCEINLINE T build_mask(const byte_sequence<Is...>&&, const BoolArgs...args)
    {
        return (T(0) | ... | (args ? (1 << Is) : 0) );
    }

public:

    template <IOOpsMode MODE, T MASK>
    static FORCEINLINE void set()
    {
        if constexpr(has_atomic_io_or || MODE == MergedOps || ((MODE == FastestOps) && (n_active_bits(MASK) > 2)))
            *C::ptr() |= MASK;
        else
            set_is(bitmask_sequence<T, MASK>{});
    }

    template <T MASK>
    static FORCEINLINE void set()
    {
        set<defaultIOOpsMode, MASK>();
    }

    template <IOOpsMode MODE, T MASK>
    static FORCEINLINE void clear()
    {
        if constexpr(has_atomic_io_and || MODE == MergedOps || (MODE == FastestOps && n_active_bits(MASK) > 2))
            *C::ptr() &= ~MASK;
        else
            clear_is(bitmask_sequence<T, MASK>{});
    }

    template <T MASK>
    static FORCEINLINE void clear()
    {
        clear<defaultIOOpsMode, MASK>();
    }

    template <IOOpsMode MODE, T MASK>
    static FORCEINLINE void toggle()
    {
        if constexpr(MODE != InterruptSafeOps)
            *C::ptr() ^= MASK;
        else
            toggle_is(bitmask_sequence<T, MASK>{});
    }

    template <T MASK>
    static FORCEINLINE void toggle()
    {
        toggle<defaultIOOpsMode, MASK>();
    }

    static FORCEINLINE void set(T byte)
    {
        *C::ptr() = byte;
    }

    // sets MASK bits if active in mask argument
    template <IOOpsMode MODE, T MASK>
    static FORCEINLINE void set_with_mask(const T mask)
    {
        if constexpr (n_active_bits(MASK) > 1 && MODE != InterruptSafeOps)
            *C::ptr() = (get() & ~MASK) | mask;
        else
            set_with_mask_is(bitmask_sequence<T, MASK>{}, mask);
    }

    template <T MASK>
    static FORCEINLINE void set_with_mask(const T mask)
    {
        set_with_mask<defaultIOOpsMode, MASK>(mask);
    }

    // sets MASK bits if true in args
    template <IOOpsMode MODE, T MASK, typename...BoolArgs>
    static FORCEINLINE void set_with_args(const BoolArgs...args)
    {
        if constexpr (n_active_bits(MASK) > 1 && MODE == MergedOps)
            set_with_mask<MASK, MODE>(build_mask(bitmask_sequence<T, MASK>{}, args...));
        else
            set_with_args_is(bitmask_sequence<T, MASK>{}, args...);
    }

    template <T MASK, typename...BoolArgs>
    static FORCEINLINE void set_with_args(const BoolArgs...args)
    {
        set_with_args<defaultIOOpsMode, MASK>(args...);
    }

    static FORCEINLINE T get()
    {
        return *C::ptr();
    }
};


template <typename T, uintptr_t BYTE_ADDR>
struct MemVar : MemVarBase<T, MemVar<T,BYTE_ADDR>>
{
    static FORCEINLINE volatile T *ptr()
    {
        return reinterpret_cast<volatile T *>(BYTE_ADDR);
    }
};

template <uintptr_t BYTE_ADDR>
using MemVar8 = MemVar<uint8_t, BYTE_ADDR>;

template <uintptr_t BYTE_ADDR>
using MemVar16 = MemVar<uint16_t, BYTE_ADDR>;
