#pragma once
#include <avr/io.h>
#include "macros.h"

#ifndef F_CPU
#error F_CPU undefined
#endif


struct Uart
{
    enum BitStop{ OneBitStop=0, TwoBitStop=1 };
    enum NumBits{ FiveBits=0, SixBits=1, SevenBits=2, EightBits=3, NineBits=7};
    enum Parity { NoParity=0, EvenParity=2, OddParity=3 };
    enum Polarity { TXOnRise=0, TXOnFall=1, NoPolarity };
    enum Mode { Asynchronous=0, Synchronous=1, Master=3 };


    static void putc(char c)
    {
        // Do nothing until UDR is ready for more data to be written to it
        while ((UCSR0A & (1 << UDRE0)) == 0) {}
        UDR0 = c;
    }

    static char getc()
    {
        // Do nothing until data have been received and is ready to be read from UDR
        while ((UCSR0A & (1 << RXC0)) == 0) {};
        return UDR0; // Fetch the received data
    }

    static void init
        ( bool enabletx
        , bool enablerx
        , int32_t baudrate
        , NumBits nbit
        , Parity parity
        , BitStop bitStop
        , Polarity pol
        , Mode mode
        )
    {
        // polarity can be set for synchronous mode only
        STATIC_ASSERT(invalid_configuration,(pol==NoPolarity || mode==Synchronous));

        // Turn on the transmission and reception circuitry, as requested
        UCSR0B |= (enablerx ? (1 << RXEN0) : 0) | (enabletx ? (1 << TXEN0) : 0);

        /* Set frame format: asynchronous, 8data, no parity, 1stop bit */
        UCSR0C = (pol==NoPolarity?0:pol) | (nbit<<UCSZ00) | (bitStop <<USBS0)
            | (parity<<UPM00) | (mode<<UMSEL00);

        // FIXME: need to make decisions about double speed here
        int32_t BAUD_PRESCALE = (((F_CPU / (baudrate * 16UL))) - 1);
        UBRR0H = (BAUD_PRESCALE >> 8); // Load upper 8-bits of the baud rate value into the high byte of the UBRR register
        UBRR0L = BAUD_PRESCALE; // Load lower 8-bits of the baud rate value into the low byte of the UBRR register
    }

};
