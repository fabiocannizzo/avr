#pragma once
#include <avr/io.h>
#include "macros.h"
#include "bit_utils.h"


#ifndef F_CPU
#error F_CPU undefined
#endif


struct timer
{
    enum ClockMode { Stop, NoScale,
        Scale8, Scale64, Scale256, Scale1024,
        ExtOnFall, ExtOnRise };
    enum FastCompOnMatchMode { Disabled = 0, Normal = 2, Inverted = 3 };


    // initialize timer prescaler
    static void initClock(ClockMode sc)
    {
        TCCR0B = sc;
    }

    // initialize timer0 fo fast PWM with TOP=255
    static void fastPWM
        ( FastCompOnMatchMode aMode // pin6, OC0A, PD6
        , FastCompOnMatchMode bMode // pin5, OC0B, PD5
        , ClockMode sc
        )
    {
        DDRB |= (aMode != Disabled ? (1 << 6) : 0)
         | (bMode != Disabled ? (1 << 5) : 0);
        TCCR0A = (aMode << COM0A0) | (bMode << COM0B0) | 0x3;

        initClock(sc);
    }

    static void setA( uint8_t a )
    {
        OCR0A = a;
    }

    static void setB( uint8_t b )
    {
        OCR0B = b;
    }

    static void set_timer_0(uint8_t value)
    {
        TCNT0 = value;
    }

    static uint8_t get_timer_0()
    {
        return TCNT0;
    }

    static bool overflow_0()
    {
        return (TIFR0 & bit8(TOV0)) == 0;
    }

    static void reset_overflow_0()
    {
        TIFR0 |= bit8(TOV0);
    }

    static void set_interrupt_ovf_0(bool enable)
    {
        uint8_t mask = bit8(TOIE0);
        if(enable)
            TIMSK0 |= mask;
        else
            TIMSK0 &= ~mask;
    }
};

