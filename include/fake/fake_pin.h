#pragma once

#include "pin.h"
#include "fake_ports.h"
#include "fake_var.h"

template <typename P, uint8_t I>
using FakePin = Pin<P, I, FakeVar8>;

typedef FakePin<PortA,0> pin_a0;
typedef FakePin<PortA,1> pin_a1;
typedef FakePin<PortA,2> pin_a2;
typedef FakePin<PortA,3> pin_a3;
typedef FakePin<PortA,4> pin_a4;
typedef FakePin<PortA,5> pin_a5;
typedef FakePin<PortA,6> pin_a6;
typedef FakePin<PortA,7> pin_a7;

typedef FakePin<PortB,0> pin_b0;
typedef FakePin<PortB,1> pin_b1;
typedef FakePin<PortB,2> pin_b2;
typedef FakePin<PortB,3> pin_b3;
typedef FakePin<PortB,4> pin_b4;
typedef FakePin<PortB,5> pin_b5;
typedef FakePin<PortB,6> pin_b6;
typedef FakePin<PortB,7> pin_b7;

typedef FakePin<PortD,0> pin_d0;
typedef FakePin<PortD,1> pin_d1;
typedef FakePin<PortD,2> pin_d2;
typedef FakePin<PortD,3> pin_d3;
typedef FakePin<PortD,4> pin_d4;
typedef FakePin<PortD,5> pin_d5;
typedef FakePin<PortD,6> pin_d6;
typedef FakePin<PortD,7> pin_d7;

typedef pin_a0 pin_0;
typedef pin_a1 pin_1;
typedef pin_a2 pin_2;
typedef pin_a3 pin_3;
typedef pin_a4 pin_4;
typedef pin_a5 pin_5;
typedef pin_a6 pin_6;
typedef pin_a7 pin_7;

typedef pin_b0 pin_8;
typedef pin_b1 pin_9;
typedef pin_b2 pin_10;
typedef pin_b3 pin_11;
typedef pin_b4 pin_12;
typedef pin_b5 pin_13;
typedef pin_b6 pin_14;
typedef pin_b7 pin_15;

typedef pin_d0 pin_16;
typedef pin_d1 pin_17;
typedef pin_d2 pin_18;
typedef pin_d3 pin_19;
typedef pin_d4 pin_20;
typedef pin_d5 pin_21;
typedef pin_d6 pin_22;
typedef pin_d7 pin_23;


