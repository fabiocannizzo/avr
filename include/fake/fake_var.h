#pragma once

#include "mem_var.h"

#ifdef DEBUG
#include <iostream>
#include <iomanip>
#include <bitset>
#endif

template <uintptr_t A>
struct FakeVar8 : MemVarBase<uint8_t, FakeVar8<A>>
{
    using T = uint8_t;
    using base_t = MemVarBase<T, FakeVar8<A>>;
    template <typename F>
    static void trace(const char *op, F f, T arg)
    {
#ifdef DEBUG
        std::cout << std::hex << A << ", "
                  << std::setw(6) << op
                  << ", pre=" << std::bitset<8>{base_t::get()}
                  << ", arg=" << std::bitset<8>{arg}
                  << ", res=" << std::bitset<8>{[f](){ f(); return base_t::get(); }()}
                  << "\n";
#else
        f(arg);
#endif
    }

    template <uint8_t MASK>
    static void set()
    {
        trace("or", base_t::template set<MASK>, MASK);
    }
    template <uint8_t MASK>
    static  void clear()
    {
        trace("and", base_t::template clear<MASK>, MASK);
    }
    template <uint8_t MASK>
    static  void toggle()
    {
        trace("toggle", base_t::template toggle<MASK>, MASK);
    }
    static  void set(T byte)
    {
        trace("set", [=](){ base_t::set(byte); }, byte);
    }

    static uint8_t *ptr() { return &m_data; }
    static uint8_t m_data;
};

template <uintptr_t A>
uint8_t FakeVar8<A>::m_data;
