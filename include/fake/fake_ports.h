#pragma once

struct PortA
{
    static constexpr uintptr_t ddr = 0xA0;
    static constexpr uintptr_t port = 0XA1;
    static constexpr uintptr_t pin = 0xA2;
};

struct PortB
{
    static constexpr uintptr_t ddr = 0xB0;
    static constexpr uintptr_t port = 0xB1;
    static constexpr uintptr_t pin = 0xB2;
};

struct PortC
{
    static constexpr uintptr_t ddr = 0xC0;
    static constexpr uintptr_t port = 0xC1;
    static constexpr uintptr_t pin = 0xC2;
};

struct PortD
{
    static constexpr uintptr_t ddr = 0xD0;
    static constexpr uintptr_t port = 0xD1;
    static constexpr uintptr_t pin = 0xD2;
};
