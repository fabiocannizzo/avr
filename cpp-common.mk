.SUFFIXES:

AVR_CC=avr-g++
EXE_CC=g++

BDIR=../..
HBDIR=$(BDIR)/include
HDIRS=$(patsubst %, $(HBDIR)/%, lib avr fake)
STLDIR=$(HBDIR)/stl
LIBDIR=$(HBDIR)/lib
AVRDIR=$(HBDIR)/avr
FAKEDIR=$(HBDIR)/fake

AVR_SRC := $(wildcard *.avr.cpp)
EXE_SRC := $(wildcard *.exe.cpp)

AVR_OBJ := $(patsubst %.cpp, %.avr.obj,$(AVR_SRC))
EXE_OBJ := $(patsubst %.cpp, %.exe.obj,$(EXE_SRC))

AVR_HEX_TARGET := $(patsubst %.cpp, %.hex,$(AVR_SRC))
AVR_EEP_TARGET := $(patsubst %.cpp, %.eep,$(AVR_SRC))
EXE_TARGET := $(patsubst %.cpp, %.exe,$(EXE_SRC))

HEADERS := $(wildcard *.h) ../mem_addr.h $(foreach dir, $(HDIRS), $(wildcard $(dir)/*)) $(wildcard $(STLDIR)/*)
MKFILES := Makefile $(wildcard $(BDIR)/*.mk)

ALLDEPS=$(HEADERS) $(MKFILES)

ALL_AVR_IM := $(patsubst %.cpp, %.obj %.s %.i,$(AVR_SRC))
ALL_EXE_IM := $(patsubst %.cpp, %.obj %.s %.i,$(EXE_SRC))

CFLAGS += -std=c++17 -c -ffunction-sections -fdata-sections -I$(LIBDIR)
LFLAGS +=

AVR_CFLAGS += $(COM_CFLAGS) -Os -DF_CPU=$(CLOCK_SPEED)  -I$(STLDIR) -I../ -I$(AVRDIR)
AVR_LFLAGS += $(COM_CFLAGS) -Wl,--gc-sections

EXE_CFLAGS += $(COM_CFLAGS) -O0 -DDEBUG  -I$(FAKEDIR)
EXE_LFLAGS += $(COM_CFLAGS) -Wl,--gc-sections -lncurses

# compile, link and encode
all: $(AVR_HEX_TARGET) $(AVR_EEP_TARGET) $(EXE_TARGET)

# extract memory addresses from avr header file macros and convert them to uintptr_t
# mem_addr.h : $(HBDIR)/avr/gen_mem_addr.h $(MKFILES)
#	$(CC) $(CFLAGS) -P -E -o /dev/stdout $< | grep autogen | sed -e 's/\(.*=\).*volatile[^(]*\(.*\)/\1 \2/g' -e 's/[()]//g' -e '1i#pragma once' > $@

# $(HBDIR)/avr/avr_ports.h : mem_addr.h

%.i: %.cpp $(ALLDEPS)
	$(CC) $(CFLAGS) -P -E -o $@ $<


# assembler
%.s: %.cpp $(ALLDEPS)
	$(CC) $(CFLAGS) -S -o $@ $<

# compile
%.obj: %.cpp $(ALLDEPS)
	$(CC) $(CFLAGS) -o $@ $<

# link
%.elf: %.obj
	$(AVR_CC) $(LFLAGS) $(AVR_LFLAGS) -o $@ $<
	avr-size $@

# link
%.exe: %.obj
	$(EXE_CC) $(LFLAGS) -o $@ $< $(EXE_LFLAGS)

$(ALL_AVR_IM) : CC = $(AVR_CC)
$(ALL_AVR_IM) : CFLAGS += $(AVR_CFLAGS)

$(ALL_EXE_IM) : CC = $(EXE_CC)
$(ALL_EXE_IM) : CFLAGS += $(EXE_CFLAGS)


# encode
%.hex: %.elf
	avr-objcopy -j .text -j .data -O ihex $< $@

# encode
%.eep: %.elf
	avr-objcopy -j .eeprom --change-section-lma .eeprom=0 -O ihex $< $@

# flash
flash: $(AVR_HEX_TARGET)
	avrdude $(DFLAGS) -v -U flash:w:$<

# eeprom
eeprom: $(AVR_EEP_TARGET)
	avrdude $(DFLAGS) -v -U eeprom:w:$<

# fuses
.PHONY: fuses
fuses:
	avrdude $(DFLAGS) -v -U lfuse:r:-:i

# clean
.PHONY: clean
clean:
	rm -f *.obj *.i *.ii *.s *.elf *.hex *.exe *.eep
