SKETCH_DIRS = $(addprefix $(shell pwd)/, $(wildcard *.sketch) )

.PHONY: all $(SKETCH_DIRS)
.PHONY: clean $(SKETCH_DIRS)

all: $(SKETCH_DIRS)

clean: $(SKETCH_DIRS) 

$(SKETCH_DIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)


