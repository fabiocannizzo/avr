This is a C++ library to program AVR micro-controllers.

While AVR micro-controllers are tipically programmed in C and make extensive use of C macros, this project aim at implementing a set of template classes so that any code written is checked for errors and inconsistencies by the compiler.

The project is still at a very early stage. The whole library is in the *include* subdirectory. There is also a *stl* directory which contains a small subset of the STL library.

The main reason for writing this library was to support the implementation of some games for attiny.
In the directory  attiny84a.dev/led-arcade.sketch you can find a minimalist implementation of the games Tetris, Snake and Conway's game of life.
The challenge was to fit all of these games in the tiny memory available. I used a 3x3 array 8x8 red-led matrices, each controlled via a max7219 controller. The max7219 controllers are daisy chained in groups of 3. The link to the board design is available at
https://easyeda.com/editor#id=8e967aaabbb1440fa95a8eaa510939ff|9e0418c08fef48189257fe7b7dd9ad64|41cd9004993140a7aac00d06b3068d80

There are static tests which are implicitly run at every compilation. Some runtime tests, for non-avr specific classes, are in the *test* directory.

The main classes are _Pin_ and _MultiPin_. For instance, you can write:  
```
#!c++

    typedef Pin<PortB, 0> led1pin;  
    led1pin::set_high(); 
```
 
which generate the equivalent C instruction:  

```
#!c++

    PORTB |= 1;  
```

multiple pins can be bundled together:  

```
#!c++

    typedef Pin<PortB, 0> led1pin;  
    typedef Pin<PortB, 1> led2pin;  
    typedef Pin<PortD, 1> led3pin;  
    typedef MultiPin<led1pin, led2pin, led3pin> leds;  
    leds::set_high();  
```
 
which will generates the equivalent C instructions:     

```
#!c++

    PORTB |= 3;  
    PORTD |= 2;  
```

Typedefs for commonly used pins are predefined in *pins.h*. For example:
```
#!c++
typedef Pin<PortB,0> pin_b0;
typedef Pin<PortB,1> pin_b1;
typedef Pin<PortB,2> pin_b2;
typedef Pin<PortB,3> pin_b3;
typedef Pin<PortB,4> pin_b4;
typedef Pin<PortB,5> pin_b5;
typedef Pin<PortB,6> pin_b6;
typedef Pin<PortB,7> pin_b7;
```

The class *hc* in *74hc595.h* allows to control a *74HC595* or *SN74HC595* device. It supports *daisy chaining* and the use of multiple chains of *74HC595* devices with common *clock* and *latch*.  

There are also some classes for:  
  * *adc.h*: analog to digital conversion (adc)  
  * *timer.h*: power modulation (pwm)
  * *uart.c*: serial communications
but these have not received much attention yet.

The examples files are in the subdirectories with the extension _sketch_. The build system expects that the subdirectory contains a *Makefile* and a *cpp* file with the same name as the directory.

Notes:  
  * Only the *ATMEGA328P* and *ATTINY84A* chips are supported at the moment.
  * The C++ dialect used is C++17
  * The default compiler is avr-gcc.  
  * The build system is based on gnu make and requires some bash tools, e.g. grep, sed, find
  * The *flash-to-chip* system supports only *Arduino Uno*.

Usage:  
  * To build all examples just type *make* from the main directory  
  * To build a sketch (e.g. *blinking-led*) and flash to the *Arduino Uno*  
 
```
#!c++

   cd blinking-led.sketch  
   make flash
```
