DEV_DIRS = $(addprefix $(shell pwd)/, $(wildcard *.dev) )
TEST_DIR = tests

.PHONY: all $(DEV_DIRS)
.PHONY: clean $(DEV_DIRS) $(TEST_DIR)
.PHONY: test $(TEST_DIR)

all: $(DEV_DIRS)

test: $(TEST_DIR)

clean: $(DEV_DIRS) $(TEST_DIR)

$(DEV_DIRS) $(TEST_DIR):
	$(MAKE) -C $@ $(MAKECMDGOALS)


