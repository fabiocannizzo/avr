#include "bit_array.h"
#include "mem_var.h"
#include "bytebits.h"
#include "pin.h"
#include "multi_pin.h"

#include "fake_pin.h"
#include "assert_util.h"

#include <iostream>
#include <cstdint>
#include <bitset>
#include <sstream>

std::string format(bool x) { return x ? " TRUE" : "FALSE"; }
std::string format(uint8_t x) { std::ostringstream os; os << std::bitset<8>(x); return os.str(); }

template <typename T>
void compare(size_t i, T v1, T v2)
{
    if(v1 != v2) {
        std::cout << "index " << i << ": expected " << format(v2) << " got " << format(v1) << "\n";
        throw;
    }
}

template <typename T, uint32_t N>
void bit_array_test1()
{
    std::cout << "bit_array_test_1\n";

    uint16_t v[N];

    for (T x : v)
        x = rand() % 3;

    bit_array<N> a(v);

    for (typename bit_array<N>::length_t i = 0; i < N; ++i)
        compare<bool>(i, a.get(i), v[i]);
}

template <uint32_t N>
void bit_array_test2(bool v)
{
    std::cout << "bit_array_test_2\n";

    bit_array<N> a(v);

    for (typename bit_array<N>::length_t i = 0; i < N; ++i)
        compare<bool>(i, a.get(i), v);
}

template <uint32_t N>
void bit_array_test3()
{
    std::cout << "bit_array_test_3\n";

    uint8_t v[N];

    bit_array<N> a{};

    for (typename bit_array<N>::length_t i = 0; i < N; ++i)
        a.set(i, v[i] = rand() % 3);

    for (typename bit_array<N>::length_t i = 0; i < N; ++i)
        compare<bool>(i, a.get(i), v[i]);
}

void bit_array_test4()
{
    std::cout << "bit_array_test_4\n";
    const char v[] = "101001110100101001010010";
    const size_t N = sizeof(v) - 1;

    typedef bit_array<N> type_t;

    type_t a{v};

    for (type_t::length_t i = 0; i < N; ++i)
        compare<bool>(i, a.get(i), v[i]-'0');
}

void bit_array_test5()
{
    std::cout << "bit_array_test_5\n";
    const size_t N = 21;
    const size_t M = 3;
    #define PACK { 0xFA, 0x83, 0x82 }
    uint8_t v[3] = PACK;
    bit_array<N> a{ bit_array<N>::bit_pack{ PACK } };
    bit_array<N> b{ PACK };

    for (typename bit_array<N>::length_t i = 0; i < N; ++i) {
        compare<bool>(i, a.get(i), v[i/8] & (1 << (i % 8)));
        compare<bool>(i, b.get(i), v[i/8] & (1 << (i % 8)));
    }
}

template <int I>
struct FakeMemVar
{
    static uint8_t *ptr() { return &data; }
    static uint8_t data;
};

template <int I>
uint8_t FakeMemVar<I>::data = 0;


void byte_bits_test()
{
    std::cout << "byte_bits_test\n";

    using mem_t = MemVarBase<uint8_t, FakeMemVar<0>>;

    // test with full mask
    using bn_t = ByteBits<mem_t,0xFF>;

    bn_t::set();
    compare<uint8_t>(0xFF, bn_t::get(), 0xFF);

    bn_t::clear();
    compare<uint8_t>(0xFF, bn_t::get(), 0);

    bn_t::toggle();
    compare<uint8_t>(0xFF, bn_t::get(), 1);

    bn_t::toggle();
    compare<uint8_t>(0xFF, bn_t::get(), 0);

    bn_t::set_value(2);
    compare<uint8_t>(0xFF, bn_t::get(), 1);

    bn_t::set_value(0);
    compare<uint8_t>(0xFF, bn_t::get(), 0);

    bn_t::set_values(0,1,1,0,0,0,0,1);
    compare<uint8_t>(0xFF, bn_t::get(), 0b10000110);

    bn_t::toggle();
    compare<uint8_t>(0xFF, bn_t::get(), 0b01100001);

    bn_t::clear();
    bn_t::set_mask(0b10000110);
    compare<uint8_t>(0xFF, bn_t::get(), 0b10000110);

    // test with null mask
    uint8_t last = bn_t::get();
    using bz_t = ByteBits<mem_t,0>;

    bz_t::set();
    compare<uint8_t>(0, mem_t::get(), last);

    bz_t::clear();
    compare<uint8_t>(0, mem_t::get(), last);

    bz_t::toggle();
    compare<uint8_t>(0, mem_t::get(), last);

    bz_t::toggle();
    compare<uint8_t>(0, mem_t::get(), last);

    bz_t::set_value(2);
    compare<uint8_t>(0, mem_t::get(), last);

    bz_t::set_value(0);
    compare<uint8_t>(0, mem_t::get(), last);

    bz_t::set_values();
    compare<uint8_t>(0, mem_t::get(), last);

    bz_t::set_mask(0);
    compare<uint8_t>(0, mem_t::get(), last);

    // test with partial mask
    const uint8_t mask = 0b11001100;
    bn_t::set_mask(0b10000110);
    using bm_t = ByteBits<mem_t,mask>;

    bm_t::set();
    compare<uint8_t>(mask, bm_t::get(), mask);
    compare<uint8_t>(mask, mem_t::get(), 0b11001110);

    bm_t::clear();
    compare<uint8_t>(mask, bm_t::get(), 0);
    compare<uint8_t>(mask, mem_t::get(), 0b00000010);

    bm_t::toggle();
    compare<uint8_t>(mask, bm_t::get(), mask);
    compare<uint8_t>(mask, mem_t::get(), 0b11001110);

    bm_t::toggle();
    compare<uint8_t>(mask, bm_t::get(), 0);
    compare<uint8_t>(mask, mem_t::get(), 0b11001110);

    bm_t::set_value(2);
    compare<uint8_t>(mask, bm_t::get(), mask);
    compare<uint8_t>(mask, mem_t::get(), 0b11001110);

    bm_t::set_value(0);
    compare<uint8_t>(mask, bm_t::get(), 0);
    compare<uint8_t>(mask, mem_t::get(), 0b00000010);

    bm_t::set_values(0,1,1,0);
    compare<uint8_t>(mask, bm_t::get(), 0b10001000);
    compare<uint8_t>(mask, mem_t::get(), 0b10001010);

    bm_t::toggle();
    compare<uint8_t>(mask, bm_t::get(), 0b01000100);
    compare<uint8_t>(mask, mem_t::get(), 0b01000100);

    bm_t::clear();
    bm_t::set_mask(0b11100000);
    compare<uint8_t>(mask, bm_t::get(), 0b11000100);
    compare<uint8_t>(mask, mem_t::get(), 0b11000100);
}

void pin_test()
{
    std::cout << "pin_test\n";
    // test with full mask
    using bm_t = ByteBits<FakeVar8<pin_1::port_t::port>,0xFF>;

    size_t cnt = 0;

    pin_1::set_high();
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000010);
    compare<bool>(cnt++, pin_1::get_value(), true);

    pin_2::set_high();
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000110);
    compare<bool>(cnt++, pin_2::get_value(), true);

    pin_1::set_low();
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000100);

    pin_2::set_low();
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000000);

    pin_1::set_value(true);
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000010);

    pin_2::set_value(true);
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000110);

    pin_1::set_value(false);
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000100);

    pin_2::set_value(false);
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000000);

    pin_1::toggle();
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000010);

    pin_2::toggle();
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000110);

    pin_1::toggle();
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000100);

    pin_2::toggle();
    compare<uint8_t>(cnt++, bm_t::get(), 0b00000000);
}

void multi_pin_test()
{
    using namespace multi_pin_h;
    using namespace assert_util_h;
    std::cout << "pin_test\n";

    // PortInfo static test
    using p1_t = typename MultiPin<pin_3>::portinfo_t;
    using x1_t = PortInfoPack<PortInfo<pin_3::port_t,0b1000,0>>;
    static_assert(p1_t::nPorts == 1, "wrong number of ports" );
    static_assert(CheckSame<p1_t,x1_t>::value, "unexpected type");

    // PortInfo static test
    using p2_t = typename MultiPin<pin_3,pin_18,pin_1,pin_8,pin_21,pin_11,pin_17>::portinfo_t;
    using x2_t = PortInfoPack<PortInfo<pin_3::port_t,0b1010,2,0>, PortInfo<pin_17::port_t,0b100110,6,1,4>,PortInfo<pin_8::port_t,0b1001,3,5>>;
    static_assert(p2_t::nPorts == 3, "wrong number of ports" );
    static_assert(CheckSame<p2_t,x2_t>::value, "unexpected type");

    // test set_values
    using p3_t = MultiPin<pin_a1, pin_a2>;
    using x3_t = PortInfoPack<PortInfo<PortA,0b110,0,1>>;
    static_assert(CheckSame<typename p3_t::portinfo_t,x3_t>::value, "unexpected type");
    using bm_t = ByteBits<FakeVar8<pin_1::port_t::port>,0b00000110>;
    p3_t::set_values(0, 0);
    compare<uint8_t>(0, bm_t::get(), 0b00000000);
    p3_t::set_values(0, 1);
    compare<uint8_t>(1, bm_t::get(), 0b00000100);
    p3_t::set_values(1, 0);
    compare<uint8_t>(2, bm_t::get(), 0b00000010);
    p3_t::set_values(1, 1);
    compare<uint8_t>(3, bm_t::get(), 0b00000110);

    using p4_t = MultiPin<pin_a2, pin_a1>;
    using x4_t = PortInfoPack<PortInfo<PortA,0b110,1,0>>;
    static_assert(CheckSame<typename p4_t::portinfo_t,x4_t>::value, "unexpected type");
    p4_t::set_values(0, 0);
    compare<uint8_t>(4, bm_t::get(), 0b00000000);
    p4_t::set_values(0, 1);
    compare<uint8_t>(5, bm_t::get(), 0b00000010);
    p4_t::set_values(1, 0);
    compare<uint8_t>(6, bm_t::get(), 0b00000100);
    p4_t::set_values(1, 1);
    compare<uint8_t>(7, bm_t::get(), 0b00000110);
}

int main()
{
    try
    {
        bit_array_test1<uint8_t, 20>();
        bit_array_test1<bool, 17>();
        bit_array_test1<uint16_t, 65>();
        bit_array_test1<uint32_t, 43>();

        bit_array_test2<64>(0);
        bit_array_test2<32>(1);

        bit_array_test3<31>();

        bit_array_test4();

        bit_array_test5();

        pin_test();

        multi_pin_test();

        std::cout << "test ok\n";
    }
    catch(const char *n)
    {
        std::cout << "failure " << n << "\n";
    }

    return 0;
}
