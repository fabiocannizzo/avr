#include <stdlib.h>
#include <util/delay.h>
#include "uart.h"
#include "adc.h"

// print the level of darkness as a percent to the serial device

int main ()
{
	Uart::init(true, true, 19200, Uart::EightBits, Uart::NoParity,
		Uart::OneBitStop, Uart::NoPolarity, Uart::Asynchronous);

	adc::init_admux(adc::AREF, adc::Ch0, false);
	adc::set_prescaler(adc::Div128);
	adc::enable();

	uint16_t darkness=0;
	char buffer[5];
	for (;;) {
		_delay_ms(500);
		darkness = adc::triggerReadFull();
		itoa( darkness*100ul/1023, buffer, 10 );
		for ( char *s = buffer; *s != 0; ++s )
			Uart::putc( *s );
		Uart::putc( '%' );
		Uart::putc( '\r' );
		Uart::putc( '\n' );
	}

	return 1;
}
