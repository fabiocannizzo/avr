#include <util/delay.h>
#include "74hc595.h"
#include "avr_pin.h"

int main (void)
{
    typedef hc595<pin_b3, pin_b4, pin_b5, 8, true> hc_t;
    typedef hc_t::buffer_t buffer_t;

    hc_t::init();

    // flash 8 pins in sequence
    buffer_t cfg[8] = {{{1}}, {{2}}, {{4}}, {{8}}, {{16}}, {{32}}, {{64}}, {{128}}};

    while (true)
    {
        for (uint8_t i = 0; i < 8; ++i) {
            hc_t::output(cfg[i]);
            _delay_ms(250);
        }
    }

    return 1;
}
