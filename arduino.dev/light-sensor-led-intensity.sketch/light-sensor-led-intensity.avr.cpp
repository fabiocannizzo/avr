#include <util/delay.h>
#include "adc.h"
#include "timer.h"

int main (void)
{
	timer::fastPWM( timer::Normal, timer::Disabled, timer::Scale8 );

	adc::init_admux(adc::AREF, adc::Ch0, true);
    adc::set_prescaler(adc::Div128);
    adc::enable();

	while (1)
	{
		uint8_t r = adc::triggerReadLeft();
		timer::setA( r );
		_delay_ms(100);
	}

	return 1;
}
