#pragma once
constexpr uintptr_t __autogen__PORTB = 0x05 + 0x20;
constexpr uintptr_t __autogen__DDRB = 0x04 + 0x20;
constexpr uintptr_t __autogen__PINB = 0x03 + 0x20;
constexpr uintptr_t __autogen__PORTC = 0x08 + 0x20;
constexpr uintptr_t __autogen__DDRC = 0x07 + 0x20;
constexpr uintptr_t __autogen__PINC = 0x06 + 0x20;
constexpr uintptr_t __autogen__PORTD = 0x0B + 0x20;
constexpr uintptr_t __autogen__DDRD = 0x0A + 0x20;
constexpr uintptr_t __autogen__PIND = 0x09 + 0x20;
