#include "avr_pin.h"

/*
Test of digital input using a button
Digital pin2 is connected to ground, but, when the button
is pressed it gets pulled up to 5V
We read its value and, if high, we light up the led connected to pin 9
*/

int main (void)
{
	pin_2::set_input();
	pin_9::set_output();

	while (1)
	{
		pin_9::set_value( pin_2::read() );
	}

	return 1;
}
