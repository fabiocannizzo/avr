#include "Arduino.h"
#include <util/delay.h>

const int n_leds = 4;
const uint8_t led_pins[n_leds] = {0, 1, 2, 3};

const uint8_t btn_left = 4;
const uint8_t btn_right = 5;

const int off   = 0;
const int left  = 1;
const int right = 2;
const int full  = 3;

int mode;
int current;

// everything off
void go_off()
{
    for (int i=0; i < n_leds; ++i)
        digitalWrite(led_pins[i], LOW);
}

// everything on
void go_full()
{
    for (int i=0; i < n_leds; ++i)
        digitalWrite(led_pins[i], HIGH);
}

// sequence from right to left
void go_left()
{
    digitalWrite(led_pins[current], LOW);
    current = current+1;
    if (current == n_leds)
        current = 0;
    digitalWrite(led_pins[current], HIGH);
}

// sequence from left to right
void go_right()
{
    digitalWrite(led_pins[current], LOW);
    current = current - 1;
    if (current < 0)
        current = n_leds - 1;
    digitalWrite(led_pins[current], HIGH);
}


void setup()
{

    for (int i=0; i < n_leds; ++i)
        pinMode(led_pins[i], OUTPUT);
    pinMode(btn_left, INPUT);
    pinMode(btn_right, INPUT);
    mode = off;
    go_off();
}

void loop()
{
    int b1 = digitalRead(btn_left);
    int b2 = digitalRead(btn_right);
    int v = b1 + 2 * b2;
    if(v == 0) {
        if(mode != off) {
            go_off();
            mode = off;
        }
    } else if(v == 1) {
        if(mode != left) {
            go_off();  // turn off all leds
            current = 0;
            mode = left;
        }
        go_left(); // move to the left
    } else if(v == 2) {
        if(mode != right) {
            go_off(); // turn off all leds
            current = n_leds - 1;
            mode = right;
        }
        go_right(); // move to the right
    } else {  // v == 3
        if(mode != full) {
            go_full();
            mode = full;
        }
    }

    _delay_ms(200);
}
