#pragma once
#include <avr/io.h>

struct DigiPortInfo
{
    typedef decltype(PORTB) port_t;
    typedef decltype(DDRB)  ddr_t;
    typedef decltype(PINB)  pin_t;
    DigiPortInfo(port_t p, ddr_t d, pin_t v)
        : port(p), ddr(d), pin(v) {}
    port_t port;
    ddr_t ddr;
    pin_t pin;
};

DigiPortInfo digi_ports[]
   { DigiPortInfo(PORTB, DDRB, PINB)    // 0
   , DigiPortInfo(PORTD, DDRD, PIND)    // 1
   };

enum DigiPortCode { DIGIB=0, DIGID=1 };

struct DigiPinInfo
{
    DigiPinInfo(DigiPortCode p, uint8_t o)
        : code(p), offset(o) {}
    DigiPortCode code;
    uint8_t offset;
};

DigiPinInfo digi_pins[]
   { DigiPinInfo(DIGID, 0)    // 0
   , DigiPinInfo(DIGID, 1)    // 1
   , DigiPinInfo(DIGID, 2)    // 2
   , DigiPinInfo(DIGID, 3)    // 3
   , DigiPinInfo(DIGID, 4)    // 5
   , DigiPinInfo(DIGID, 5)    // 6
   };

const uint8_t INPUT = 0;
const uint8_t OUTPUT = 1;
const uint8_t LOW = 0;
const uint8_t HIGH = 1;

// convenient macros
#define SET_HI(P,B) P |= (1 << B)
#define SET_LO(P,B) P &= ~(1 << B)
#define GET_PIN(P,B) (P & (1 << B))


void pinMode(uint8_t pin, uint8_t d)
{
    const DigiPinInfo &p = digi_pins[pin];
    DigiPortInfo &m = digi_ports[p.code];
    if (d == INPUT)
        SET_LO(m.ddr, p.offset);
    else
        SET_HI(m.ddr, p.offset);
}

void digitalWrite(uint8_t pin, uint8_t d)
{
    const DigiPinInfo &p = digi_pins[pin];
    DigiPortInfo &m = digi_ports[p.code];
    if (d == LOW)
        SET_LO(m.port, p.offset);
    else
        SET_HI(m.port, p.offset);
}

int digitalRead(uint8_t pin)
{
    const DigiPinInfo &p = digi_pins[pin];
    DigiPortInfo &m = digi_ports[p.code];
    return GET_PIN(m.pin, p.offset)? HIGH: LOW;
}


void setup();
void loop();

int main()
{
    setup();
    while(true)
        loop();
    return 0;
}
