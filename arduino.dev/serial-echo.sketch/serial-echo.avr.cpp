#include "uart.h"

int main ()
{
	Uart::init(true, true, 19200, Uart::EightBits, Uart::NoParity,
			Uart::OneBitStop, Uart::NoPolarity, Uart::Asynchronous );

	for (;;) {
		Uart::putc(Uart::getc());
	}

	return 1;
}
