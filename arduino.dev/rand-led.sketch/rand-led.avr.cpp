#include <util/delay.h>
#include <stdlib.h>

#include "multi_pin.h"
#include "avr_pin.h"

uint8_t rand_bool()
{
	return static_cast<uint8_t>( rand() & 0x1 );
}

int main (void)
{
	typedef MultiPin<pin_d0, pin_b1, pin_b0, pin_d1> dpinsu;

	dpinsu::set_output();

	constexpr char z = 0;
	constexpr char u = 1;

	while (u)
	{
		dpinsu::set_values(z, z, z, z);
		_delay_ms(400);
		dpinsu::set_values(u, z, z, z);
		_delay_ms(400);
		dpinsu::set_values(z, u, z, z);
		_delay_ms(400);
		dpinsu::set_values(z, z, u, z);
		_delay_ms(400);
		dpinsu::set_values(z, z, z, u);
		_delay_ms(1000);
		for (char i = z; i < 10; ++i) {
			dpinsu::set_values(z, rand_bool(), z, rand_bool());
			_delay_ms(250);
		}
	}

	return u;
}
