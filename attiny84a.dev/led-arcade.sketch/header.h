#pragma once
#include "rand_util.h"
#include "button.h"
#include "pgmspacewrap.h"

// global variables
extern XorWow rndgen;

// functions
uint8_t read_adc(bool row, uint8_t MI, uint8_t MA, uint8_t x, int8_t& dx);
uint8_t read_adc(bool row, uint8_t MI, uint8_t MA, uint8_t x);

void my_delay(uint8_t nticks);
ButtonEvent btnGetEventAndReset(uint8_t eventMask);
void btnReset();

// font
enum font_index_t { FI_0, FI_1, FI_2, FI_3, FI_4, FI_5, FI_6, FI_7, FI_8, FI_9,
                 FI_D, FI_G, FI_L, FI_M, FI_P, FI_S, FI_T, FI_W,
                 FI_PERC, FI_LROUND, FI_RROUND, FI_INFTY, FI_SQUARE, FI_LARROW
               };

extern const uint8_t fonts[FI_LARROW + 1][5] PROGMEM;

uint8_t menu_select(const font_index_t *items, uint8_t nmenu, uint8_t index);


const uint8_t maxNumDigitScore = 4;
uint8_t getDigits(uint16_t v, uint8_t buffer[maxNumDigitScore]);

