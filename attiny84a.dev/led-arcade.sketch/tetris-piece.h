#pragma once

struct TetrisPiece {

    uint8_t m_bits[2];

    static uint8_t getIndex(uint8_t row)
    {
        return row / 2;
    }

    static uint8_t getMask(uint8_t row, uint8_t col)
    {
        if (row & 1)
            col += 4;
        return 1 << col;
    }

    bool getBlk(uint8_t row, uint8_t col) const
    {
        return m_bits[getIndex(row)] & getMask(row, col);
    }

    void setBlk(uint8_t row, uint8_t col, bool value)
    {
        if (value)
            m_bits[getIndex(row)] |= getMask(row, col);
        else
            m_bits[getIndex(row)] &= ~getMask(row, col);
    }
};
