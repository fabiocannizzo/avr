#pragma once
#include "header.h"
#include "hash.h"

typedef uint8_t hash_t;

struct HashBuffer
{
    static const uint8_t M = 3;         // minimum number of repetitions to decide it is boring
    static const uint8_t N = 80;        // number of consecutive board positions hashes stored

    HashBuffer()
        : m_size(0)
    {
    }

    bool boring(uint8_t new_hash)
    {
        bool boring = false;

        for (uint8_t i = 0; !boring && i + (M - 1) < m_size; ++i) {
            // check for lag i + 1
            if (new_hash == m_hashes[i]) { // is lag i+1 a candidate?
                boring = true;
                for (uint8_t j = 0; j < M - 1; ++j)
                    if (m_hashes[j] != m_hashes[1 + i + j]) {
                        boring = false;
                        break;
                    }
            }
        }

        // add new_hash at the front of m_hashes
        for (uint8_t i = N - 1; i > 0; --i)
            m_hashes[i] = m_hashes[i - 1];
        m_hashes[0] = new_hash;

        // increase size, if not at full size already
        if (m_size < N)
            ++m_size;

        return boring;
    }

private:
    uint8_t m_size = 0;
    hash_t m_hashes[N];
};

template <typename Matrix, typename PIN>
struct GameOfLife
{
    Matrix& matrix;
    bool inf_board;
    uint8_t prob_level;

    ButtonEvent game()
    {
        HashBuffer hashes;
        PIN::set_low();
        while(true) {
            matrix.refresh();
            gol_step(inf_board);
            PIN::set_value(hashes.boring(hash()));
            for (uint8_t i = 0; i < 4; ++i) {
                if(ButtonEvent be = btnGetEventAndReset(Click | DblClick); be)
                    return be;
                //my_delay(75);
            }
        }
    }

    void setup()
    {
        const uint8_t nRows = Matrix::nRows;
        const uint8_t nCols = Matrix::nCols;

        uint8_t cur_row = 0, cur_col = 0;
        bool cur_value = 0;
        int8_t dc = 0, dr = 0;
        matrix.set_all(false, true);

        while(true) {

            // flash
            for (uint8_t i = 1; i < 3; ++i) {
                matrix.set_led(cur_row, cur_col, i & 1, true);
                my_delay(60);
            }

            // check button
            ButtonEvent be = btnGetEventAndReset(Click | DblClick);
            if (be == DblClick) {
                matrix.set_led(cur_row, cur_col, cur_value, true); // reset flashing pixel
                return;
    	    }
            if (be == Click) {
                cur_value = !cur_value;
            }

            PIN::toggle();

            // check joystick
            uint8_t new_row = read_adc(true, 0, nRows - 1, cur_row, dr);
            uint8_t new_col = read_adc(false, 0, nCols - 1, cur_col, dc);

            if ((new_row != cur_row) || (new_col || cur_col)) {
                matrix.set_led(cur_row, cur_col, cur_value, true);
                cur_value = matrix.get_led(new_row, new_col);
                cur_col = new_col;
                cur_row = new_row;
            }
        }
    }


    void choose_prob()
    {
        matrix.set_all(false);

        // display prob header
        const font_index_t header[] =  // P(1)
            { FI_P
            , FI_LROUND
            , FI_1
            , FI_RROUND
            };
        for (uint8_t i = 0, c = 1; i < sizeof(header) / sizeof(*header); ++i, c += 6)
            matrix.loadFont(3, c, header[i]);

        uint8_t new_prob_level = prob_level++;
        do {
            if(prob_level != new_prob_level) {
                prob_level = new_prob_level;
                uint8_t buffer[maxNumDigitScore];
                getDigits((prob_level * 100u) / 16, buffer);
                uint8_t c = 3;
                for (uint8_t i = 2; i < maxNumDigitScore; ++i, c += 6)
                    matrix.loadFont(14, c, (font_index_t) buffer[i]);
                matrix.loadFont(14, c, FI_PERC);
                matrix.refresh();
            }
            my_delay(100);
            new_prob_level = 16 - read_adc(true, 1, 15, 16 - prob_level);
        } while (!btnGetEventAndReset(Click));
    }

    uint8_t gol_menu_select()
    {
        const font_index_t items[] =
            { FI_G
            , FI_M
            , FI_P
            , !inf_board ? FI_INFTY : FI_SQUARE
            , FI_LARROW
            };
        const uint8_t nmenu = sizeof(items) / sizeof(*items);
        return menu_select(items, nmenu, 0);
    }


    // prob must be in the range [1..15]
    void do_randomize(uint8_t prob)
    {
        RandGen<15> gen4bits{};
        for (uint8_t c = 0; c < Matrix::nCols; ++c)
            for (uint8_t r = 0; r < Matrix::nRows; ++r)
                matrix.set_led(r, c, gen4bits(rndgen) < prob);
    }


    void gol_step(bool inf_board)
    {
        // this implementation uses more RAM, as it makes a copy of the matrix,
        // but it is much simplere and generates a smaller code
        Matrix matcopy{matrix};

        for (uint8_t r = 0; r < Matrix::nRows; ++r)  {
            for (uint8_t c = 0; c < Matrix::nCols; ++c)  {
                uint8_t sum = 0;

                uint8_t i = r - 1;
                do {
                    uint8_t j = c - 1;
                    do {
                        if (i == r && c == j)
                            continue;

                        uint8_t ii = i, jj = j;
                        if (inf_board) {
                            if (ii == uint8_t(0xFF))
                                ii = Matrix::nRows - 1;
                            else if (ii == Matrix::nRows)
                                ii = 0;
                            if (jj == uint8_t(0xFF))
                                jj = Matrix::nCols - 1;
                            else if (jj == Matrix::nCols)
                                jj = 0;
                        }

                        if (ii < Matrix::nRows && jj < Matrix::nCols)
                            sum += matcopy.get_led(ii, jj);

                    } while(++j <= c + 1);

                } while (++i <= r + 1);

                if (sum < 2 || sum >=3)
                    matrix.set_led(r, c, sum == 3);
            }
        }
    }

    hash_t hash()
    {
        return pjw_hash<hash_t, uint8_t>(matrix.getBuffer(), Matrix::buffer_size);
    }


    GameOfLife(Matrix& m)
        : matrix(m)
        , inf_board(false)
        , prob_level(4)
    {
    }

    void run()
    {
        bool randomize = true;
        while(true) {

            if(randomize)
                do_randomize(prob_level);

            auto g = game();
            randomize = true;

            if (g == DblClick) {
                switch(gol_menu_select()) {
                    case 0:  // randomized game
                        break;
                    case 1:  // manual setup game
                        setup();
                        randomize = false;
                        break;
                    case 2:
                        choose_prob();
                        break;
                    case 3:
                        inf_board = !inf_board;
                        break;
                    default:
                        return;  // back to main menu
                };
            }
        }
    }

};
