#pragma once
#include "header.h"
#include "tetris-piece.h"
#include <algorithm>

template <typename Matrix>
struct Tetris
{
    static const uint8_t height = 23;
    static const uint8_t width = 10;
    static const uint8_t row_offset = 0; // board row offset with respect to top row
    static const uint8_t col_offset = 1; // board column offset with respect to first column

    static const uint8_t new_piece_offset = width / 2 + 6;

    static_assert(col_offset >= 1
        && row_offset + height < Matrix::nRows
        && col_offset + width + 1 < Matrix::nCols
        );

    enum Moves {DoNothing = 0, Left = 0x1, Right = 0x2, Clock = 0x4, AntiClock = 0x8, DropOne = 0x10, DropAll=0x20};
    enum Collision {NoCollision = 0, LeftBorder = 0x1, RightBorder = 0x2, Other = 0x4};

    struct ActivePiece
    {
        int8_t m_r, m_c;
        uint8_t m_piece;
        uint8_t m_rot;
    };

    Matrix& matrix;
    RandGen<7 * 4 - 1> m_rnd;

    static const uint8_t s_pieces[7][4][2];
    TetrisPiece m_pieces[7][4];
    ActivePiece m_cur, m_nxt;
    uint16_t m_score, m_nPieces;

    Moves getMove()
    {
#ifdef __AVR__
        switch (read_adc(false, 0, 2, 1)) {
            case 0: btnReset();  return Left;
            case 2: btnReset(); return Right;
            default: break;
        };
        switch (read_adc(true, 0, 2, 1)) {
            case 0: btnReset(); return AntiClock;
            case 2: btnReset(); return DropAll;
            default: break;
        };

//        if (btnGetEventAndReset(Click))
//            return DropAll;

        return DoNothing;
#else
        switch (getch()) {
            case 'l': return Left;
            case 'r': return Right;
            //case 'c': return Clock;
            case 'a': return AntiClock;
            case 'd': return DropAll;
            case '1': return DropOne;
            case 'q': exit(0);
            default: return DropOne;
        };
#endif
    }

    void draw()
    {
        m_nxt.m_r = row_offset;
        m_nxt.m_c = col_offset + (width - 4) / 2 + new_piece_offset;
        uint8_t r = m_rnd(rndgen);
        m_nxt.m_rot = r / 7;
        m_nxt.m_piece = r % 7;
    }

    // place piece on the board
    void place(const ActivePiece& apc, bool visible)
    {
        const TetrisPiece& pc = m_pieces[apc.m_piece][apc.m_rot];
        for (uint8_t r = 0; r < 4; ++r)
            if (r + apc.m_r >= row_offset)
                for (uint8_t c = 0; c < 4; ++c)
                    if (pc.getBlk(r, c))
                        matrix.set_led(r + apc.m_r, c + apc.m_c, visible);
    }

    // collide with other pieeces on the board?
    Collision collision()
    {
        const TetrisPiece& pc = m_pieces[m_cur.m_piece][m_cur.m_rot];
        for (uint8_t r = 0; r < 4; ++r)
            if (r + m_cur.m_r >= row_offset)
                for (uint8_t c = 0; c < 4; ++c)
                    if (pc.getBlk(r, c) && matrix.get_led(r + m_cur.m_r, c + m_cur.m_c))
                        switch(c + m_cur.m_c) {
                            case (col_offset - 1): return LeftBorder;
                            case (col_offset + width): return LeftBorder;
                            default: return Other;
                        };
        return NoCollision;
    }


    Tetris(Matrix& m)
        : matrix(m)
        , m_score(0)
        , m_nPieces(0)
    {
        // clear the board
        matrix.set_all(false);

        // draw borders
        for (uint8_t i = row_offset; i < row_offset + height + 1; ++i) {
            matrix.set_led(i, col_offset - 1, true);
            matrix.set_led(i, col_offset + width, true);
        }
        for (uint8_t i = col_offset - 1; i < col_offset + width + 1; ++i) {
            matrix.set_led(row_offset + height, i, true);
        }

        // load pieces
        for (uint8_t p = 0; p < 7; ++p)
            for (uint8_t rot = 0; rot < 4; ++rot) {
                m_pieces[p][rot].m_bits[0] = pgm_read_byte(&s_pieces[p][rot][0]);
                m_pieces[p][rot].m_bits[1] = pgm_read_byte(&s_pieces[p][rot][1]);
            }

        // draw next piece
        draw();

        // show
        matrix.refresh();
    }

    // play the game
    bool newPiece()
    {
        place(m_nxt, false);
        m_cur = m_nxt;
        m_cur.m_c -= new_piece_offset;

        draw();
        place(m_nxt, true);

        // check collision
        if (collision())
            return false; // game over

        // place piece on the board
        ++m_nPieces;
        place(m_cur, true);

        matrix.refresh();
        return true;
    }

    Collision move(uint8_t m)
    {
        if (m == DoNothing)
            return NoCollision;

        ActivePiece backup(m_cur);
        place(m_cur, false);

        if (m & Left)       --m_cur.m_c;
        if (m & Right)      ++m_cur.m_c;
        if (m & AntiClock)  m_cur.m_rot = (m_cur.m_rot + 1) % 4;
        //if (m & Clock)      { if (m_cur.m_rot) --m_cur.m_rot; else m_cur.m_rot = 3; }
        if (m & DropOne)    ++m_cur.m_r;

        Collision ret = collision();

        if (ret != NoCollision)
            m_cur = backup;

        place(m_cur, true);

        return ret;
    }

    bool fullrow(uint8_t r) const
    {
        bool full = true;
        for (uint8_t c = col_offset; c < col_offset + width; ++c)
            if (!matrix.get_led(r, c)) {
                full = false;
                break;
            }
        return full;
    }

    void clearrow(uint8_t r)
    {
        for (uint8_t c = col_offset; c < col_offset + width; ++c)
            matrix.set_led(r, c, false);
    }

    void moverow(uint8_t src, uint8_t dst)
    {
        for (uint8_t c = col_offset; c < col_offset + width; ++c)
            matrix.set_led(dst, c, matrix.get_led(src, c));
    }

    // play the game
    uint16_t play()
    {
        const uint8_t nInitialTicks = 6;
        uint8_t nTicks = nInitialTicks;
        const uint8_t nPolls = 10;
        const uint8_t nPieces = 40;   // number of pieces before a tick increment
        const uint8_t pollDelay = 10; // in my-delay ticks (2ms)
        while (newPiece()) {
            bool noTouchDown = true;
            while (noTouchDown) {
                for (uint8_t i = 0; i <= nTicks; ++i) {
                    Moves m = DoNothing;
                    for (uint8_t j = 0; j < nPolls; ++j) {
                        if(m == DoNothing)
                            m = (i == nTicks) ? DropOne : getMove();
                        my_delay(pollDelay);
                    }
                    if (m == DropAll) {
                        while (NoCollision == move(DropOne));
                        m_score += (row_offset + height - m_cur.m_r) / 5;
                        noTouchDown = false;
                        i = nTicks;  // exit for loop
                    }
                    else if(m == DropOne) {
                        noTouchDown = NoCollision == move(m);
                        i = nTicks;  // exit for loop
                    }
                    else {
                        Collision c = move(m);
                        if(m & (Clock | AntiClock)) {
                            if (c == LeftBorder)
                                move(m | Right); // bounce right
                            else if(c == RightBorder)
                                move(m | Left);
                        }
                    }

                    matrix.refresh();
                }
            }

            my_delay(pollDelay * nPolls);
            m_nPieces++;
            nTicks = nInitialTicks - std::min(uint8_t(m_nPieces / nPieces), uint8_t(nInitialTicks - 1));

            // check for completed lines
            uint8_t rkeep, rcheck;
            bool changed = false;
            for (rcheck = row_offset + height, rkeep = rcheck; rcheck-- > row_offset;) {
                if (!fullrow(rcheck)) {
                    --rkeep;
                    if (rcheck != rkeep) {
                        moverow(rcheck, rkeep);
                        changed = true;
                    }
                }
                else {
                    m_score += 10;
                }
            }
            if (changed) {
                while (rkeep-- > row_offset) {
                    clearrow(rkeep);
                }
                matrix.refresh();
            }
        }
        return m_score;
    }
};

template <typename M>
const uint8_t Tetris<M>::s_pieces[7][4][2] PROGMEM =
{
#include "tetris-bitmaps.h"
};
