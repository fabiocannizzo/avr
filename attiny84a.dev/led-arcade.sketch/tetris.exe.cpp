#include "rand_util.h"

#include "board_matrix.h"
#include "ncurses_matrix.h"
#include "tetris.h"
#include <cstdlib>
#include <ctime>

using Matrix = BoardMatrix<NCursesMatrix>;
Matrix matrix;

XorWow rndgen;

struct dummy_pin
{
    static void set_high() {};
    static void set_low() {};
    static void toggle() {};
};

uint8_t rrand()
{
    return uint8_t(((int) rand()) < 0);
}

void my_delay(uint8_t nticks)
{
    //clock_t dt = CLOCKS_PER_SEC / 1000 * 2 * nticks;
    //std::clock_t t0 = std::clock();
    //while ((std::clock() - t0) < dt);
}

int main(void)
{
    rndgen.seed(rrand);
    char ch;
    matrix.status_bar(1, "l: move left");
    matrix.status_bar(2, "r: move right");
    matrix.status_bar(3, "c: rotate clock wise");
    matrix.status_bar(4, "a: rotate anti clock wise");
    matrix.status_bar(5, "d: drop all");
    matrix.status_bar(6, "1: drop one");
    matrix.status_bar(7, "q : quit");
    Tetris<Matrix> tetris{ matrix };
    tetris.play();
    return 0;
}
