#pragma once
#include "header.h"

template <typename Matrix>
struct Snake
{
    static const uint8_t vertical_mask      = 0b01000;
    static const uint8_t horizontal_mask    = 0b00100;
    static const uint8_t undefined_mask     = 0b10000;
    static const uint8_t neutral_mask       = 0b00001;
    static const uint8_t dec_mask           = 0b00000;
    static const uint8_t inc_mask           = 0b00010;

    // we rely on this bitmaps to determine vertical or horizontal movement
    enum Direction
        { Up   = vertical_mask | dec_mask
        , Down = vertical_mask | inc_mask
        , Left = horizontal_mask | dec_mask
        , Right = horizontal_mask | inc_mask
        , Undefined = undefined_mask
        };

    // direction in compressed format, for storage
    enum ZippedDirection
        { ZUp   =  0b00
        , ZDown =  0b01
        , ZLeft =  0b10
        , ZRight = 0b11
        };

    Matrix& matrix;
    uint16_t len;
    uint8_t row_head, col_head, row_tail, col_tail, row_food, col_food;
    uint8_t direction[Matrix::nRows * Matrix::nCols * 2 / 8]; // to store the direction of the next piece

    static bool vertical(Direction d)    { return d & vertical_mask; }
    static bool horizontal(Direction d)  { return d & horizontal_mask; }
    static bool undefined(Direction d)   { return d & undefined_mask; }

    static ZippedDirection zipDirection(Direction d)
    {
        return ZippedDirection((d & (inc_mask | horizontal_mask)) >> 1);
    }

    // position the food in a box not already occupied by the snake
    void newfood()
    {
#if 1
        uint16_t nFreeCells = Matrix::nRows * Matrix::nCols - len;
        uint16_t rnd = uint16_t(rndgen() % nFreeCells);
        // search by row until we found the rnd-th cell
        for (uint8_t c = 0; c < Matrix::nCols; ++c)
            for (uint8_t r = 0; r < Matrix::nRows; ++r)
                if(!matrix.get_led(r, c) && (rnd-- == 0)) {
                    row_food = r;
                    col_food = c;
                    // we could terminate the loop early, but prefer a predictable execution time
                }
#else
        do {
            row_food = rand() % Matrix::nRows;
            col_food = rand() % Matrix::nCols;
        } while(matrix.get_led(row_food, col_food));

#endif
        matrix.set_led(row_food, col_food, true);
    }

    // position the head and tail of the snake of length 1
    // and set the first poistion of the food
    Snake(Matrix& m)
        : matrix(m)
        , len(1)
        , row_head(Matrix::nRows / 2)
        , col_head(Matrix::nCols / 2)
        , row_tail(Matrix::nRows / 2)
        , col_tail(Matrix::nCols / 2)
    {
        // clear the board
        matrix.set_all(false);

        // mark the position as occupied in the board
        matrix.set_led(row_head, col_head, true);

        // new food position
        newfood();

        matrix.refresh();
    }

    bool move(ZippedDirection newDir)
    {
        // set new direction of head
        uint8_t curDir = direction[0];
        curDir &= ~uint8_t(0b11);
        curDir |= newDir;
        direction[0] = curDir;

        // compute new head coordinates
        uint8_t row_head_new = row_head, col_head_new = col_head;
        switch(newDir) {
            case ZUp:
                if (row_head_new > 0) {
                    --row_head_new;
                    break;
                }
                else
                    return false;  // hit the wall: game over
            case ZDown:
                if (row_head_new < Matrix::nRows - 1) {
                    ++row_head_new;
                    break;
                }
                else
                    return false;  // hit the wall: game over
            case ZLeft:
                if (col_head_new > 0) {
                    --col_head_new;
                    break;
                }
                else
                    return false;  // hit the wall: game over
            case ZRight:
                if (col_head_new < Matrix::nCols - 1) {
                    ++col_head_new;
                    break;
                }
                else
                    return false;  // hit the wall: game over
        };

        if(row_head_new != row_food || col_head_new != col_food) {  // not reached the food?

            if(matrix.get_led(row_head_new, col_head_new))
               return false;  // byting its own tail, game over

            // move the tail in head position
            matrix.set_led(row_tail, col_tail, false);
            matrix.set_led(row_head_new, col_head_new, true);

            // update tail position
            uint8_t tail_index = len - 1;
            uint8_t tail_byte_index = tail_index / 4;
            uint8_t tail_byte_offset = (tail_index % 4) * 2;
            uint8_t byte_dir = 0b11 & (direction[tail_byte_index] >> tail_byte_offset);
            auto tailDir = ZippedDirection(byte_dir);
            switch(tailDir) {
                case ZUp:
                    --row_tail;
                    break;
                case ZDown:
                    ++row_tail;
                    break;
                case ZLeft:
                    --col_tail;
                    break;
                case ZRight:
                    ++col_tail;
                    break;
            };
        }
        else {  // reached the food: eat
            if (++len >= Matrix::nRows*Matrix::nCols)
                return false;  //filled the entire board: game over
            newfood();
        }

        // update directions
        uint8_t lsb = newDir;
        // shift left the direction array by two bits and add the direction at the head
        for (uint16_t i = 0; i < sizeof(direction); ++i) {
            uint8_t dirs = direction[i];
            uint8_t tmp = dirs >> 6;    // save most significant two bits, which will be the lest significant two bits in the next byte
            dirs <<= 2;                 // shit left
            dirs |= lsb;                // insert new direction at the head
            direction[i] = dirs;        // update first byte
            lsb = tmp;                  // this will be inserted at the begin of the next byte
        }

        // update head position
        row_head = row_head_new;
        col_head = col_head_new;

        matrix.refresh();

        return true;
    }

    // play the game
    uint16_t play()
    {
        Direction curdir = Undefined;
        do {
            uint8_t count = 0;
            auto olddir = curdir;
            do {
                uint8_t newdir = neutral_mask;
                if(!vertical(olddir))
                    newdir = read_adc(true, 0, 2, 1) | vertical_mask;
                if(!horizontal(olddir) && (newdir & neutral_mask))
                    newdir = read_adc(false, 0, 2, 1) | horizontal_mask;
                // the adc can return 0, 1, or 2. If it returned 1, there is no change of direction.
                if(!(newdir & neutral_mask))
                    curdir = Direction(newdir);
                my_delay(15);
            } while(++count < 8 || undefined(curdir));
        } while(move(zipDirection(curdir)));
        return len;
    }
};

