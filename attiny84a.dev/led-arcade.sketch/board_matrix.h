#pragma once

#include "header.h"

// we use 1088B, which has anode on rows and cathod on columns
// each max7219 address a 8x1 column of leds
// we daisy chain along rows, hence chains (pins) correspond to rows
template <typename M>
struct BoardMatrix : M
{
    typedef M base_t;

    void loadFont(uint8_t row, uint8_t col, font_index_t i)
    {
        for (uint8_t dig = 0; dig < 5; ++dig) {
            uint8_t b = pgm_read_byte(&fonts[i][dig]);
            for (uint8_t r = 0; r < 7; ++r, b >>= 1)
                base_t::set_led(row + r, col + dig, b & 1);
        }
    }

    void underline(uint8_t r, uint8_t c, bool on)
    {
        for (uint8_t i = 0; i < 5; ++i)
            base_t::set_led(r, c + i, on);
    }

    void _show_score(uint8_t row, uint16_t score)
    {
        uint8_t buffer[maxNumDigitScore];
        uint8_t ndig = getDigits(score, buffer);
        uint8_t msglen = 5 + (ndig - 1) * 6;
        uint8_t col1 = (base_t::nCols - msglen) / 2;
        uint8_t i = maxNumDigitScore - ndig;
        do {
            loadFont(row,  col1, font_index_t(buffer[i]));
            col1 += 6;
        } while(++i < maxNumDigitScore);
    }

    void show_score(uint16_t bestscore, uint16_t score)
    {
        base_t::set_all(false);
        uint8_t row1 = (base_t::nRows - 15) / 2;
        _show_score(row1, bestscore);
        _show_score(row1 + 8, score);
        base_t::refresh();
    }


};
