#include "rand_util.h"

#include "board_matrix.h"
#include "ncurses_matrix.h"
#include "gol.h"
#include <cstdlib>

using Matrix = BoardMatrix<NCursesMatrix>;
Matrix matrix;

XorWow rndgen;

struct dummy_pin
{
    static void set_high() {};
    static void set_low() {};
    static void toggle() {};
};

uint8_t rrand()
{
    return uint8_t(((int) rand()) < 0);
}

int main(void)
{
    rndgen.seed(rrand);
    char ch;
    matrix.status_bar(1, "q: quit");
    matrix.status_bar(2, "n: new game");
    do {
        GameOfLife<Matrix, dummy_pin> gol{ matrix };
        gol.do_randomize(4);
        HashBuffer hashes;
        do {
            matrix.refresh();
            gol.gol_step(false);
            matrix.status_bar(0,hashes.boring(gol.hash()) ? "boring!" : "");
            ch = getch();
        } while (ch != 'q' && ch != 'n');
    } while (ch == 'n');
    return 0;
}
