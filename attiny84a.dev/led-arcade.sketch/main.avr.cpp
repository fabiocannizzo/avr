#include "max7219.h"
#include "multi_pin.h"
#include "hash.h"
#include "avr_pin.h"
#include "spi_bit_bang.h"
#include "timer.h"
#include "adc.h"
#include "button.h"

#include "snake.h"
#include "gol.h"
#include "tetris.h"
#include "board_matrix.h"

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/eeprom.h>


// typedefs

typedef BitBang
    < pin_b0 // clk
    , MultiPin<pin_a3,pin_a2,pin_a1> // data
    , pin_b1 // load
    , false   // fast
    > tx;

const uint8_t nDevPerChain = 3;

typedef max7219
    < tx
    , 8  // no. column per device
    , nDevPerChain  // no. device per chain
    , false // reverse dev
    , true  // reverse digits
    , true  // reverse segments
    > led_driver;

// we use 1088B, which has anode on rows and cathod on columns
// each max7219 address a 8x1 column of leds
// we daisy chain along rows, hence chains (pins) correspond to rows
//using Matrix = led_driver::template Matrix<true, true>;

using button_t = Button<120>;

// global variables
Debouncer<6,false> deb{};
button_t btn{};
//volatile uint8_t timer_tick = 0;

// constants
const adc::Channel row_adc_ch = adc::Ch0;
const adc::Channel col_adc_ch = adc::Ch7;

// random number generator
XorWow rndgen;

// FIXME: this must be in the same order as the array below.
// it would be better to use macros

const uint8_t fonts[][5] PROGMEM = {
    {0b00111110,0b01010001,0b01001001,0b01000101,0b00111110}, // 0
    {0b00000000,0b01000010,0b01111111,0b01000000,0b00000000}, // 1
    {0b01000010,0b01100001,0b01010001,0b01001001,0b01000110}, // 2
    {0b00100001,0b01000001,0b01000101,0b01001011,0b00110001}, // 3
    {0b00011000,0b00010100,0b00010010,0b01111111,0b00010000}, // 4
    {0b00100111,0b01000101,0b01000101,0b01000101,0b00111001}, // 5
    {0b00111100,0b01001010,0b01001001,0b01001001,0b00110000}, // 6
    {0b00000011,0b01110001,0b00001001,0b00000101,0b00000011}, // 7
    {0b00110110,0b01001001,0b01001001,0b01001001,0b00110110}, // 8
    {0b00000110,0b01001001,0b01001001,0b00101001,0b00011110}, // 9
    {0b01111111,0b01000001,0b01000001,0b01000001,0b00111110}, // D
    {0b00111110,0b01000001,0b01001001,0b01001001,0b00111010}, // G
    {0b01111111,0b01000000,0b01000000,0b01000000,0b01000000}, // L
    {0b01111111,0b00000010,0b00001100,0b00000010,0b01111111}, // M
    {0b01111111,0b00001001,0b00001001,0b00001001,0b00000110}, // P
    {0b01000110,0b01001001,0b01001001,0b01001001,0b00110001}, // S
    {0b00000001,0b00000001,0b01111111,0b00000001,0b00000001}, // T
    {0b00111111,0b01000000,0b00111000,0b01000000,0b00111111}, // W
    {0b00100011,0b00010011,0b00001000,0b01100100,0b01100010}, // %
    {0b00000000,0b00011100,0b00100010,0b01000001,0b00000000}, // (
    {0b00000000,0b01000001,0b00100010,0b00011100,0b00000000}, // )
    {0b00001100,0b00010010,0b00001100,0b00010010,0b00001100}, // infty
    {0b00111110,0b00100010,0b00100010,0b00100010,0b00111110}, // square
    {0b00001000,0b00011100,0b00101010,0b00001000,0b00001000}, // left-arrow
};


void my_delay(uint8_t n_timer_ticks)
{
    while(n_timer_ticks--)
        _delay_ms(2);
    //const uint8_t target = timer_tick + n_timer_ticks;
    //while(timer_tick != target);
}

ButtonEvent btnGetEventAndReset(uint8_t eventmask)
{
    return btn.getEventAndReset(eventmask);
}

void btnReset()
{
    btn.reset();
}


uint8_t getDigits(uint16_t v, uint8_t buffer[maxNumDigitScore])
{
    uint8_t nsd = 1; // number of significant digits
    uint8_t n = maxNumDigitScore;   // read 4 digits maximum
    do {
        uint8_t digit = v % 10;
        v /= 10;
        buffer[--n] = digit;
        if (digit)
            nsd = 4 - n;
    } while(n);
    return nsd;
}

// we use 1088B, which has anode on rows and cathod on columns
// each max7219 address a 8x1 column of leds
// we daisy chain along rows, hence chains (pins) correspond to rows
using Matrix = BoardMatrix<led_driver::template Matrix<true, true>>;
Matrix matrix;


template <bool invert = true>
uint8_t _read_adc(uint8_t MI, uint8_t MA, adc::Channel ch, uint8_t x, int8_t& dx)
{
    adc::switch_channel(ch);
    uint8_t val = adc::triggerReadLeft() >> 5;
    if (invert)
        val = 7 - val;
    if (val < 3) {
        if (x > MI) {
            --x;
            if (dx >= 0)
                dx = -1;
            else if(dx > -3)
                --dx;
            else if(x > MI)
                --x;
        }
        else
            dx = 0;
    }
    else if(val > 4) {
        if (x < MA) {
            ++x;
            if (dx <= 0)
                dx = 1;
            else if(dx < 3)
                ++dx;
            else if(x < MA)
                ++x;

        }
        else
            dx = 0;
    }
    else
        dx = 0;

    return x;
}

uint8_t read_adc(bool row, uint8_t MI, uint8_t MA, uint8_t x, int8_t& dx)
{
    return _read_adc(MI, MA, row ? row_adc_ch : col_adc_ch, x, dx);
}

uint8_t read_adc(bool row, uint8_t MI, uint8_t MA, uint8_t x)
{
    int8_t dx = 0;
    return read_adc(row, MI, MA, x, dx);
}


ISR(TIM0_OVF_vect)
{
    //++timer_tick;
    btn.update(deb.update(pin_b2::read()));
}

void init()
{
    pin_a4::set_output(); // led
    pin_a4::set_high();

    // give time to capacitors to charge
    my_delay(100);

    // configure timer and enable interrupt
    timer::set_timer_0(0);
    timer::set_interrupt_ovf_0(true);
    sei();
    timer::initClock(timer::Scale8);

     // joystick button
    pin_b2::set_input();

    led_driver::init();

    // enable adc
    adc::set_prescaler(adc::Div8);
    adc::enable();

    // generate random seed and init random generator
    XorWow::state_t s;
    adc::init_admux(adc::AVcc, adc::IREF1_1, false);
    my_delay(2);
    rndgen.seed(adc::triggerReadFull);
/*
    do {
        for (uint8_t i = 0; i < 24; ++i) {
            for (uint8_t j = 0; j < 16; ++j)
                matrix.set_led(i, j, v & bit8(j));
            matrix.refresh();
            pin_a4::toggle();
            _delay_ms(100);
        }
    } while(1);
*/

    // configure adc voltage reference to read joystick potentiometers
    adc::init_admux(adc::AVcc, row_adc_ch, true);
    adc::triggerReadLeft(); // dummy read


    // power save: shutdown USI and TIMER1
    PRR |= bit8(1) | bit8(3);
}


void diagnostic()
{
    const uint8_t nRows = Matrix::nRows;
    const uint8_t nCols = Matrix::nCols;

    // flash full board
    bool go = true;
    while(go) {
        bool on = true;
        for (uint8_t i = 0; go && i < 4; ++i) {
            matrix.set_all(on, true);
            pin_a4::set_value(on);
            my_delay(254);
            on = !on;
            go = Click != btnGetEventAndReset(Click | DblClick);
        }
        for (uint8_t i = 0; go && i < nRows; ++i) {
            for (uint8_t j = 0; j < nCols; ++j)
                matrix.set_led(i, j, true);
            matrix.refresh();
            my_delay(80);
            for (uint8_t j = 0; j < nCols; ++j)
                matrix.set_led(i, j, false);
            go = Click != btnGetEventAndReset(Click | DblClick);
        }
        for (uint8_t i = 0; go && i < nCols; ++i) {
            for (uint8_t j = 0; j < nRows; ++j)
                matrix.set_led(j, i, true);
            matrix.refresh();
            my_delay(80);
            for (uint8_t j = 0; j < nRows; ++j)
                matrix.set_led(j, i, false);
            go = Click != btnGetEventAndReset(Click | DblClick);
        }
    }
}

uint8_t menu_select(const font_index_t *items, uint8_t nmenu, uint8_t index)
{
    auto row = [](uint8_t i){ return 1 + 10 * (i / 3); };
    auto col = [](uint8_t i){ return 1 +  8 * (i % 3); };

    matrix.set_all(false);

    for (uint8_t i = 0; i < nmenu; ++i)
        matrix.loadFont(row(i), col(i), items[i]);

    uint8_t newIndex = index;
    bool first = true;
    do {
        if(first || index != newIndex) {
            first = false;
            matrix.underline(row(index) + 8, col(index), false);
            index = newIndex;
            matrix.underline(row(index) + 8, col(index), true);
            matrix.refresh();
        }
        newIndex = read_adc(false, 0, nmenu - 1, index);
        my_delay(100);
    } while (!btnGetEventAndReset(Click));
    return index;
}



uint8_t main_menu_select(uint8_t sel = 0)
{
    const font_index_t items[] =
        { FI_L
        , FI_S
        , FI_T
        , FI_W
        , FI_D
        };
    const uint8_t nmenu = sizeof(items) / sizeof(*items);
    return menu_select(items, nmenu, sel);
}

uint16_t scores[4] EEMEM = {};



// the template parameter Game must have:
// - a default constructor
// - a play() method returning the score
template <template <typename M> class G>
static void run_game()
{
    using Game = G<Matrix>;

    constexpr uint8_t score_index = std::is_same<Game, Snake<Matrix>>::value ? 0 : 1;
    uint16_t best_score = eeprom_read_word(&scores[score_index]);

    bool newGame = true;
    do {
         // initialize and run the game
        Game game{matrix};

        // play the game
        uint16_t score = game.play();
        if (score > best_score) {
            best_score = score;
            eeprom_write_word(&scores[score_index], best_score);
        }

        // show score
        my_delay(250);

        matrix.show_score(best_score, score);

        // wait for user decision
        bool waitEvent = true;
        btn.reset();  // reset, in case we clicked accidentally while playing
        do {
            switch(btnGetEventAndReset(Click | DblClick)) {
                case DblClick:
                    newGame = false; // back to main menu
                case Click:
                    waitEvent = false;
                    break;
                default:
                    break;
            };
        } while(waitEvent);
    } while(newGame) ;
}


int main (void)
{
    // init pins, adc and timer
    init();

    diagnostic();

    auto sel = 0;
    while(true) {
        switch(sel = main_menu_select(sel)) {
            case 0: GameOfLife<Matrix,pin_a4>{matrix}.run(); break;
            case 1: run_game<Snake>(); break;
            case 2: run_game<Tetris>(); break;
            case 3: break;
            default: diagnostic(); break;
        };
    }

    return 1;
}
