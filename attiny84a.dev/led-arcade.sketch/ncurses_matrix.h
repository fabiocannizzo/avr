#pragma once
#include "bit_matrix.h"

#include <ncurses.h>
#include <iostream>
#include <iomanip>

struct NCursesMatrix
{
    static const uint8_t nRows = 24;
    static const uint8_t nCols = 24;
    bit_matrix<nRows, nCols> m_leds;

    NCursesMatrix()
    {
        initscr();              // Start curses mode
        raw();                  // Line buffering disabled
        keypad(stdscr, TRUE);   // We get F1, F2 etc..
        noecho();               // Don't echo() while we do getch
        curs_set(0);            // do not display cursor

        // draw square around the board
        for (uint8_t r = 0; r < nRows+2; ++r) {
            mvprintw(r, 0, "|");
            mvprintw(r, nCols + 1, "|");
        }
        for (uint8_t r = 0; r < nCols + 2; ++r) {
            mvprintw(0, r, "-");
            mvprintw(nRows+1, r, "-");
        }

    }

    ~NCursesMatrix()
    {
       endwin();           /* End curses mode        */
    }

    bool get_led(uint8_t r, uint8_t c) const { return m_leds.get(r, c); }
    void set_led(uint8_t r, uint8_t c, bool v, bool s = false)
    {
        m_leds.set(r, c, v);
        if (s)
            refresh();
    }
    void set_all(bool v) { m_leds.set_all(v); }

    uint8_t *getBuffer() { return m_leds.data(); }
    static const uint8_t buffer_size = sizeof(m_leds);
/*
    void refresh() const
    {
        std::cout << std::dec << "     ";
        for (uint8_t c = 0; c < nCols; ++c)
            std::cout << std::setw(2) << 1u + c << " ";
        std::cout << std::endl;
        for (uint8_t r = 0; r < nRows; ++r) {
            std::cout << std::setw(2) << (1u + r) << ": ";
            for (uint8_t c = 0; c < nCols; ++c)
                std::cout << std::setw(3) << get_led(r, c);
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }
*/
    void refresh() const
    {
         for (uint8_t r = 0; r < nRows; ++r) {
             for (uint8_t c = 0; c < nCols; ++c)
                 if(get_led(r,c))
                     set(r+1,c+1);
                 else
                     clear(r+1,c+1);
	}
    }

    void status_bar(uint8_t n, const char *msg)
    {
        mvprintw(nRows + 2 + n, 0, "%-20 s", msg);
    }

private:
    void clear(uint8_t r, uint8_t c) const
    {
        mvprintw(r, c, " ");
    }

    void set(uint8_t r, uint8_t c) const
    {
        mvprintw(r, c, "*");
    }
};
