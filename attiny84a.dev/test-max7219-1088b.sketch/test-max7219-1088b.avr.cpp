#include <util/delay.h>

#include "max7219.h"
#include "multi_pin.h"
#include "avr_pin.h"
#include "spi_bit_bang.h"

// Test a red led matrix 1088b (anodes on leds) and a max7219 led driver.
// In addition we have a single led flashing, just to check the MCU is running.
// Connections:
//   max7219 clock: max7219
//   max7219 data: pin_b0
//   max7219 latch: pin_a7
//   single led: pin_a1
//   column i (i=[1...7]) of the 1088b led matrix connected to DIG i-1 of the max7219
//   row 1 of the 1088b led matrix connected to SEGDP of the max7219
//   row i (i=[2..7]) of the 1088b led matrix connected to SEGA,SEGB,...,SEGG of the max7219
// The program flash the matrix, then scano led by led by row, the scan led by led by columns

typedef BitBang
    < pin_a3 // clk
    , pin_b0 // data
    , pin_a7 // load
    , true   // fast
    > tx;

typedef max7219
    < tx
    , 8  // no. column per device
    , 1  // no. device per chain
    , false // reverse dev
    , false // reverse digits
    , true  // reverse segments
    > led_driver;

led_driver::Matrix<true, true> matrix;

typedef pin_a1 led;

void pause()
{
    _delay_ms(250);
}

int main (void)
{
    // init
    led::set_output();
    led_driver::init();

    while(true) {
        bool on = on;

        // flash whole matrix
        for (uint8_t i = 0; i < 4; ++i) {
            matrix.set_all(on =! on, true);
            led::toggle();
            pause();
        }

        // flash rows
        for (uint8_t r = 0; r < 8; ++r) {
            for (uint8_t i = 0; i < 2; ++i) {
                for (uint8_t c = 0; c < 8; ++c)
                    matrix.set_led(r, c, on);
                on = !on;
                led::toggle();
                matrix.refresh();
                pause();
            }
        }

        // flash columns
        for (uint8_t c = 0; c < 8; ++c) {
            for (uint8_t i = 0; i < 2; ++i) {
                for (uint8_t r = 0; r < 8; ++r)
                    matrix.set_led(r, c, on);
                on = !on;
                led::toggle();
                matrix.refresh();
                pause();
            }
        }

    }

    return 1;
}
