#include <util/delay.h>

#include "multi_pin.h"
#include "avr_pin.h"

int main (void)
{
	typedef MultiPin<pin_a1, pin_a2> dpins;

	/* set pins for output*/
	dpins::set_output();

	while (1)
	{
		dpins::set_high();
		_delay_ms(200);
		dpins::set_low();
		_delay_ms(200);
		dpins::set_value(1);
		_delay_ms(200);
		dpins::set_value(0);
		_delay_ms(1000);
	}

	return 1;
}
