#pragma once
constexpr uintptr_t __autogen__PORTA = 0x1B + 0x20;
constexpr uintptr_t __autogen__DDRA = 0x1A + 0x20;
constexpr uintptr_t __autogen__PINA = 0x19 + 0x20;
constexpr uintptr_t __autogen__PORTB = 0x18 + 0x20;
constexpr uintptr_t __autogen__DDRB = 0x17 + 0x20;
constexpr uintptr_t __autogen__PINB = 0x16 + 0x20;
